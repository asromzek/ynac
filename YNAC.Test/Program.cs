﻿using System;
using YNAC.Financier;

namespace YNAC.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create new budget.
            var Budget = new FinancierBudget("Test Budget", CURRENCY.USD, DateTime.Now);

            // Add accounts.
            var Checking = Budget.AddOnBudgetAccount(ACCOUNT_ON_BUDGET.DEBIT, "Checking Account", null, true);
            var Savings = Budget.AddOnBudgetAccount(ACCOUNT_ON_BUDGET.SAVINGS, "Savings Account", "Goal: $1,000,000");
            var Investment = Budget.AddOffBudgetAccount(ACCOUNT_OFF_BUDGET.INVESTMENT, "Investment Account");
            var Mortgage = Budget.AddOffBudgetAccount(ACCOUNT_OFF_BUDGET.MORTGAGE, "Mortgage");
            var Car = Budget.AddOffBudgetAccount(ACCOUNT_OFF_BUDGET.LOAN, "Car Loan", "Paid off!", false, true);

            // Add master categories.
            var Bills = Budget.AddMasterCategory("Bills");
            var Food = Budget.AddMasterCategory("Food");
            var Transportation = Budget.AddMasterCategory("Transportation");

            // Add categories.
            var Rent = Budget.AddCategory(Bills, "Rent");
            var Electric = Budget.AddCategory(Bills, "Electric");
            var Cable = Budget.AddCategory(Bills, "Cable");

            var Groceries = Budget.AddCategory(Food, "Groceries");
            var Restaurants = Budget.AddCategory(Food, "Restaurants");
            var FastFood = Budget.AddCategory(Food, "Fast Food");

            var Fuel = Budget.AddCategory(Transportation, "Fuel");
            var CarLoan = Budget.AddCategory(Transportation, "Car Loan");
            var CarInsurance = Budget.AddCategory(Transportation, "Car Insurance");

            // Add budget amounts.
            var Rent_2016_11 = Budget.AddMonthBudget(Rent, 60000, 2016, 11);
            var Electric_2016_11 = Budget.AddMonthBudget(Electric, 15000, 2016, 11);
            var Cable_2016_11 = Budget.AddMonthBudget(Cable, 7500, 2016, 11);

            // Add payees.
            var StartBalance = Budget.AddPayee("Start Balance");
            var Walmart = Budget.AddPayee("Walmart");
            var Kroger = Budget.AddPayee("Kroger");
            var Speedway = Budget.AddPayee("Speedway");

            // Add transactions.
            Checking.AddIncome(StartBalance, new DateTime(2016, 11, 1), 100000, "Starting balance.", true, false, FLAG.GREEN);
            Savings.AddIncome(StartBalance, new DateTime(2016, 11, 1), 0, "Starting balance.", true, false, FLAG.GREEN);
            Investment.AddTransaction(StartBalance, new DateTime(2016, 11, 1), 50000, "Starting balance.", true, false, FLAG.GREEN);
            Mortgage.AddTransaction(StartBalance, new DateTime(2016, 11, 1), -500000, "Starting balance.", true, false, FLAG.RED);
            Checking.AddTransferToOffBudget(FastFood, false, false, FLAG.RED, Investment, false, false, FLAG.GREEN, new DateTime(2016, 11, 1), 3000, "Transfer from on to off budget.");

            // Add a split.
            var WalmartSplit = Checking.AddSplitTransaction(Walmart, new DateTime(2016, 11, 1), "Split transaction", false, false, FLAG.BLUE);
            WalmartSplit.AddSplitTransferOnBudget(Savings, false, false, FLAG.RED, 2500, null, "Transfer to on budget account.");
            WalmartSplit.AddSplitTransferOffBudget(CarLoan, Mortgage, false, false, FLAG.YELLOW, -2500, null, "Transfer to off budget account.");
            WalmartSplit.AddSplit(Fuel, 2500, null, "Regular split transaction.");
            WalmartSplit.AddSplitIncome(2500, null, "Income");

            // Write budget file to hard drive.
            //using (var Writer = new StreamWriter(@"C:\budget.json"))
            //    Writer.Write(Budget.BuildJson());

            // Display budget output in console.
            //Console.WriteLine(Budget.BuildJson());
            //Console.ReadKey();
        }
    }
}
