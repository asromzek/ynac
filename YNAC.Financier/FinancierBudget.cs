﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using YNAC.Financier.Document;

namespace YNAC.Financier
{
    //####################################################################################################
    //  Budget builder class.
    //####################################################################################################

    public class FinancierBudget
    {
        //====================================================================================================
        //  Budget objects that will be grouped together generate a valid JSON document.
        //====================================================================================================

        public BudgetRoot Root { get; }
        public BudgetOpened Opened { get; }
        public List<IAccount> Accounts { get; }
        public List<MasterCategory> MasterCategories { get; }
        public List<Category> Categories { get; }
        public List<MonthCategory> MonthCategories { get; }
        public List<Payee> Payees { get; }
        public List<ITransaction> Transactions { get; }

        //====================================================================================================
        //  Constructor.
        //====================================================================================================

        public FinancierBudget(string name, CURRENCY currency, DateTime created)
        {
            Root = new BudgetRoot(this, name, currency, created);
            Opened = new BudgetOpened(Root, created);
            Accounts = new List<IAccount>();
            MasterCategories = new List<MasterCategory>();
            Categories = new List<Category>();
            MonthCategories = new List<MonthCategory>();
            Payees = new List<Payee>();
            Transactions = new List<ITransaction>();
        }

        //====================================================================================================
        //  Assemble the collection of objects and serialize into one giang JSON document.
        //====================================================================================================

        public string BuildJson()
        {
            var Documents = new List<IDocument>();

            Documents.Add(Root);
            Documents.Add(Opened);

            foreach (var Doc in Accounts)
                Documents.Add(Doc);

            foreach (var Doc in MasterCategories)
                Documents.Add(Doc);

            foreach (var Doc in Categories)
                Documents.Add(Doc);

            foreach (var Doc in MonthCategories)
                Documents.Add(Doc);

            foreach (var Doc in Payees)
                Documents.Add(Doc);

            foreach (var Doc in Transactions)
                Documents.Add(Doc);

            return JsonConvert.SerializeObject(Documents, Formatting.Indented);
        }

        //====================================================================================================
        //  Add on budget account.
        //====================================================================================================

        public AccountOnBudget AddOnBudgetAccount(ACCOUNT_ON_BUDGET type, string name, string note = null, bool checkNumber = false, bool closed = false)
        {
            var Account = new AccountOnBudget(this, type, name, note, checkNumber, closed);
            Account.sort = Accounts.Count;
            Accounts.Add(Account);
            return Account;
        }

        //====================================================================================================
        //  Add off budget account.
        //====================================================================================================

        public AccountOffBudget AddOffBudgetAccount(ACCOUNT_OFF_BUDGET type, string name, string note = null, bool checkNumber = false, bool closed = false)
        {
            var Account = new AccountOffBudget(this, type, name, note, checkNumber, closed);
            Account.sort = Accounts.Count;
            Accounts.Add(Account);
            return Account;
        }

        //====================================================================================================
        //  Add master category.
        //====================================================================================================

        public MasterCategory AddMasterCategory(string name)
        {
            var MasterCategory = new MasterCategory(Root, name);
            MasterCategory.sort = MasterCategories.Count;
            MasterCategories.Add(MasterCategory);
            return MasterCategory;
        }

        //====================================================================================================
        //  Add category.
        //====================================================================================================

        public Category AddCategory(MasterCategory masterCategory, string name)
        {
            var Category = new Category(Root, masterCategory, name);
            Category.sort = Categories.Count;
            Categories.Add(Category);
            return Category;
        }

        //====================================================================================================
        //  Add month/category budget amount.
        //====================================================================================================

        public MonthCategory AddMonthBudget(Category category, int budget, int year, int month, bool? overspending = null, string note = null)
        {
            var MonthCategory = new MonthCategory(Root, category, budget, year, month, overspending, note);
            MonthCategories.Add(MonthCategory);
            return MonthCategory;
        }

        //====================================================================================================
        //  Add payee.
        //====================================================================================================

        public Payee AddPayee(string name)
        {
            var Payee = new Payee(Root, name);
            Payees.Add(Payee);
            return Payee;
        }
    }
}
