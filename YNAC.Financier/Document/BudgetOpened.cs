﻿using Newtonsoft.Json;
using System;

namespace YNAC.Financier.Document
{
    //####################################################################################################
    //  Budget opened class.
    //####################################################################################################

    public class BudgetOpened : IDocument
    {
        //====================================================================================================
        //  Non-serializatble properties.
        //====================================================================================================

        [JsonIgnore]
        public BudgetRoot Root { get; set; }

        //====================================================================================================
        //  Serializable properties.
        //====================================================================================================

        public string opened { get; set; }
        public string _id { get { return $"budget-opened_{Root.Uuid}"; } }

        //====================================================================================================
        //  Constructor.
        //====================================================================================================

        public BudgetOpened(BudgetRoot root, DateTime opened)
        {
            Root = root;
            this.opened = opened.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
        }
    }
}
