﻿using System.Collections.Generic;

namespace YNAC.Financier.Document
{
    //####################################################################################################
    //  Transaction interface.
    //####################################################################################################

    public interface ITransaction : IDocument
    {
        //====================================================================================================
        //  Non-serializatble properties.
        //====================================================================================================

        FinancierBudget Budget { get; }
        IAccount Account { get; }
        Category Category { get; }
        CATEGORY_TYPE Type { get; }
        Payee Payee { get; }
        string Uuid { get; }

        //====================================================================================================
        //  Serializable properties.
        //====================================================================================================

        int value { get; }
        string date { get; }
        string category { get; }
        string account { get; }
        string memo { get; }
        bool cleared { get; }
        bool reconciled { get; }
        string flag { get; }
        string payee { get; }
        string transfer { get; }
        List<TransactionSplit> splits { get; }
    }
}
