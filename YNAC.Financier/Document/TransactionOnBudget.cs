﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace YNAC.Financier.Document
{
    //####################################################################################################
    //  Transaction class.
    //####################################################################################################

    public class TransactionOnBudget : ITransaction
    {
        //====================================================================================================
        //  Non-serializatble properties.
        //====================================================================================================

        [JsonIgnore]
        public FinancierBudget Budget { get; }

        [JsonIgnore]
        public IAccount Account { get; }

        [JsonIgnore]
        public Payee Payee { get; }

        [JsonIgnore]
        public Category Category { get; }

        [JsonIgnore]
        public CATEGORY_TYPE Type { get; }

        [JsonIgnore]
        public DateTime Date { get; }

        [JsonIgnore]
        public int Value { get; }

        [JsonIgnore]
        public string Uuid { get; }

        //====================================================================================================
        //  Serializable properties.
        //====================================================================================================

        public int value
        {
            get
            {
                if (Type == CATEGORY_TYPE.SPLIT)
                {
                    var Sum = 0;

                    foreach (var SplitTransaction in splits)
                        Sum += SplitTransaction.value;

                    return Sum;
                }
                else
                    return Value;
            }
        }
        public string date { get { return Date.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); } }
        public string category
        {
            get
            {
                if (Category != null)
                    return Category.Uuid;
                else
                {
                    switch (Type)
                    {
                        case CATEGORY_TYPE.SPLIT:
                            return "split";
                        case CATEGORY_TYPE.INCOME:
                            return "income";
                        case CATEGORY_TYPE.INCOME_NEXT_MONTH:
                            return "incomeNextMonth";
                        default:
                            return null;
                    }
                }
            }
        }
        public string account { get { return Account.Uuid; } }
        public string memo { get; }
        public bool cleared { get; }
        public bool reconciled { get; }
        public string flag { get; }
        public string payee { get { return (Payee != null) ? Payee.Uuid : null; } }
        public string transfer { get; }
        public List<TransactionSplit> splits { get; }
        public string _id { get { return $"b_{Budget.Root.Uuid}_transaction_{Uuid}"; } }

        //====================================================================================================
        //  Transaction without category constructors.
        //====================================================================================================

        public TransactionOnBudget (
            FinancierBudget budget,
            AccountOnBudget account,
            Payee payee,
            Category category,
            CATEGORY_TYPE type,
            DateTime date,
            int value,
            string memo,
            bool cleared,
            bool reconciled,
            FLAG flag,
            string uuid = null,
            string transfer = null)
        {
            if (uuid != null)
                Uuid = uuid;
            else
                Uuid = Guid.NewGuid().ToString();

            Budget = budget;
            Account = account;
            Payee = payee;
            Category = category;
            Type = type;
            Date = date;
            Value = value;

            this.memo = memo;
            this.cleared = cleared;
            this.reconciled = reconciled;
            this.transfer = transfer;

            switch (flag)
            {
                case FLAG.RED:
                    this.flag = "#ff0000";
                    break;
                case FLAG.ORANGE:
                    this.flag = "#faa710";
                    break;
                case FLAG.YELLOW:
                    this.flag = "#e5e500";
                    break;
                case FLAG.GREEN:
                    this.flag = "#76b852";
                    break;
                case FLAG.BLUE:
                    this.flag = "#5276b8";
                    break;
                case FLAG.PURPLE:
                    this.flag = "#b852a9";
                    break;
                default:
                    this.flag = null;
                    break;
            }

            splits = new List<TransactionSplit>();
        }

        //====================================================================================================
        //  Add on budget split sub-transaction.
        //====================================================================================================

        public TransactionSplit AddSplit(Category category, int value, Payee payee = null, string memo = null)
        {
            var Split = new TransactionSplit(this, payee, category, CATEGORY_TYPE.NORMAL, value, memo);
            splits.Add(Split);
            return Split;
        }

        //====================================================================================================
        //  Add on budget split income sub-transaction.
        //====================================================================================================

        public TransactionSplit AddSplitIncome(int value, Payee payee = null, string memo = null)
        {
            var Split = new TransactionSplit(this, payee, null, CATEGORY_TYPE.INCOME, value, memo);
            splits.Add(Split);
            return Split;
        }

        //====================================================================================================
        //  Add on budget split income next month sub-transaction.
        //====================================================================================================

        public TransactionSplit AddSplitIncomeNextMonth(int value, Payee payee = null, string memo = null)
        {
            var Split = new TransactionSplit(this, payee, null, CATEGORY_TYPE.INCOME_NEXT_MONTH, value, memo);
            splits.Add(Split);
            return Split;
        }

        //====================================================================================================
        //  Add on budget split transfer sub-transaction.
        //====================================================================================================

        public Tuple<TransactionSplit, TransactionOnBudget> AddSplitTransferOnBudget(AccountOnBudget dstAccount, bool dstCleared, bool dstReconciled, FLAG dstFlag, int value, Payee payee = null, string memo = null)
        {
            var SourceUuid = Guid.NewGuid().ToString();
            var DestinationUuid = Guid.NewGuid().ToString();

            var Split = new TransactionSplit(this, payee, null, CATEGORY_TYPE.NORMAL, value, memo, SourceUuid, DestinationUuid);
            var DestinationTransaction = new TransactionOnBudget(Budget, dstAccount, null, null, CATEGORY_TYPE.NORMAL, Date, -value, memo, dstCleared, dstReconciled, dstFlag, DestinationUuid, SourceUuid);

            Budget.Transactions.Add(DestinationTransaction);

            splits.Add(Split);
            return Tuple.Create(Split, DestinationTransaction);
        }

        //====================================================================================================
        //  Add off budget split transfer sub-transaction.
        //====================================================================================================

        public Tuple<TransactionSplit, TransactionOffBudget> AddSplitTransferOffBudget(Category srcCategory, AccountOffBudget dstAccount, bool dstCleared, bool dstReconciled, FLAG dstFlag, int value, Payee payee = null, string memo = null)
        {
            var SourceUuid = Guid.NewGuid().ToString();
            var DestinationUuid = Guid.NewGuid().ToString();

            var Split = new TransactionSplit(this, payee, srcCategory, CATEGORY_TYPE.NORMAL, value, memo, SourceUuid, DestinationUuid);
            var DestinationTransaction = new TransactionOffBudget(Budget, dstAccount, null, null, CATEGORY_TYPE.NORMAL, Date, -value, memo, dstCleared, dstReconciled, dstFlag, DestinationUuid, SourceUuid);

            Budget.Transactions.Add(DestinationTransaction);

            splits.Add(Split);
            return Tuple.Create(Split, DestinationTransaction);
        }
    }
}
