﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace YNAC.Financier.Document
{
    //####################################################################################################
    //  Transaction class.
    //####################################################################################################

    public class TransactionOffBudget : ITransaction
    {
        //====================================================================================================
        //  Non-serializatble properties.
        //====================================================================================================

        [JsonIgnore]
        public FinancierBudget Budget { get; }

        [JsonIgnore]
        public IAccount Account { get; }

        [JsonIgnore]
        public Payee Payee { get; }

        [JsonIgnore]
        public Category Category { get; }

        [JsonIgnore]
        public CATEGORY_TYPE Type { get; }

        [JsonIgnore]
        public DateTime Date { get; }

        [JsonIgnore]
        public string Uuid { get; }

        //====================================================================================================
        //  Serializable properties.
        //====================================================================================================

        public int value { get; }
        public string date { get { return Date.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); } }
        public string category
        {
            get
            {
                if (Category != null)
                    return Category.Uuid;
                else
                {
                    switch (Type)
                    {
                        case CATEGORY_TYPE.SPLIT:
                            return "split";
                        case CATEGORY_TYPE.INCOME:
                            return "income";
                        case CATEGORY_TYPE.INCOME_NEXT_MONTH:
                            return "incomeNextMonth";
                        default:
                            return null;
                    }
                }
            }
        }
        public string account { get { return Account.Uuid; } }
        public string memo { get; }
        public bool cleared { get; }
        public bool reconciled { get; }
        public string flag { get; }
        public string payee { get { return (Payee != null) ? Payee.Uuid : null; } }
        public string transfer { get; }
        public List<TransactionSplit> splits { get; }
        public string _id { get { return $"b_{Budget.Root.Uuid}_transaction_{Uuid}"; } }

        //====================================================================================================
        //  Transaction without category constructors.
        //====================================================================================================

        public TransactionOffBudget (
            FinancierBudget budget,
            AccountOffBudget account,
            Payee payee,
            Category category,
            CATEGORY_TYPE type,
            DateTime date,
            int value,
            string memo,
            bool cleared,
            bool reconciled,
            FLAG flag,
            string uuid = null,
            string transfer = null)
        {
            if (uuid != null)
                Uuid = uuid;
            else
                Uuid = Guid.NewGuid().ToString();

            Budget = budget;
            Account = account;
            Payee = payee;
            Category = category;
            Type = type;
            Date = date;

            this.value = value;
            this.memo = memo;
            this.cleared = cleared;
            this.reconciled = reconciled;
            this.transfer = transfer;

            switch (flag)
            {
                case FLAG.RED:
                    this.flag = "#ff0000";
                    break;
                case FLAG.ORANGE:
                    this.flag = "#faa710";
                    break;
                case FLAG.YELLOW:
                    this.flag = "#e5e500";
                    break;
                case FLAG.GREEN:
                    this.flag = "#76b852";
                    break;
                case FLAG.BLUE:
                    this.flag = "#5276b8";
                    break;
                case FLAG.PURPLE:
                    this.flag = "#b852a9";
                    break;
                default:
                    this.flag = null;
                    break;
            }

            splits = new List<TransactionSplit>();
        }
    }
}
