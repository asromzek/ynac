﻿using Newtonsoft.Json;
using System;

namespace YNAC.Financier.Document
{
    //####################################################################################################
    //  Master category class.
    //####################################################################################################

    public class MasterCategory : IDocument
    {
        //====================================================================================================
        //  Non-serializatble properties.
        //====================================================================================================

        [JsonIgnore]
        public BudgetRoot Root { get; set; }

        [JsonIgnore]
        public string Uuid { get; set; }

        //====================================================================================================
        //  Serializable properties.
        //====================================================================================================

        public string name { get; }
        public int sort { get; set; }
        public bool collapsed { get; set; }
        public string _id { get { return $"b_{Root.Uuid}_master-category_{Uuid}"; } }

        //====================================================================================================
        //  Constructor.
        //====================================================================================================

        public MasterCategory(BudgetRoot root, string name)
        {
            Root = root;
            Uuid = Guid.NewGuid().ToString();

            this.name = name;
        }
    }
}
