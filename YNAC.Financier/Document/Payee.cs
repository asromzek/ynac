﻿using Newtonsoft.Json;
using System;

namespace YNAC.Financier.Document
{
    //####################################################################################################
    //  Payee class.
    //####################################################################################################

    public class Payee : IDocument
    {
        //====================================================================================================
        //  Non-serializatble properties.
        //====================================================================================================

        [JsonIgnore]
        public BudgetRoot Root { get; set; }

        [JsonIgnore]
        public string Uuid { get; set; }

        //====================================================================================================
        //  Serializable properties.
        //====================================================================================================

        public string name { get; }
        public string _id { get { return $"b_{Root.Uuid}_payee_{Uuid}"; } }

        //public bool autosuggest { get; set; }
        //public bool internal { get; set; }

        //====================================================================================================
        //  Constructor.
        //====================================================================================================

        public Payee(BudgetRoot root, string name)
        {
            Root = root;
            Uuid = Guid.NewGuid().ToString();

            this.name = name;

            //autosuggest = true;
            //internal = false;
        }
    }
}
