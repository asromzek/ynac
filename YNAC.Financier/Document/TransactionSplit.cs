﻿using Newtonsoft.Json;
using System;

namespace YNAC.Financier.Document
{
    //####################################################################################################
    //  Split class.
    //####################################################################################################

    public class TransactionSplit
    {
        //====================================================================================================
        //  Non-serializatble properties.
        //====================================================================================================

        [JsonIgnore]
        public TransactionOnBudget Transaction { get; }

        [JsonIgnore]
        public Payee Payee { get; }

        [JsonIgnore]
        public CATEGORY_TYPE Type { get; }

        [JsonIgnore]
        public Category Category { get; }

        //====================================================================================================
        //  Serializable properties.
        //====================================================================================================

        public string id { get; }
        public int value { get; }
        public string category
        {
            get
            {
                if (Category != null)
                    return Category.Uuid;
                else
                {
                    switch (Type)
                    {
                        case CATEGORY_TYPE.SPLIT:
                            return "split";
                        case CATEGORY_TYPE.INCOME:
                            return "income";
                        case CATEGORY_TYPE.INCOME_NEXT_MONTH:
                            return "incomeNextMonth";
                        default:
                            return null;
                    }
                }
            }
        }
        public string memo { get; }
        public string payee { get { return (Payee != null) ? Payee.Uuid : null; } }
        public string transfer { get; }

        //====================================================================================================
        //  Constructor.
        //====================================================================================================

        public TransactionSplit(
            TransactionOnBudget transaction,
            Payee payee,
            Category category,
            CATEGORY_TYPE type,
            int value,
            string memo,
            string uuid = null,
            string transfer = null)
        {
            if (uuid != null)
                id = uuid;
            else
                id = Guid.NewGuid().ToString();

            Transaction = transaction;
            Payee = payee;
            Category = category;
            Type = type;

            this.value = value;
            this.memo = memo;
            this.transfer = transfer;
        }
    }
}
