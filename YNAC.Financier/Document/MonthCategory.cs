﻿using Newtonsoft.Json;

namespace YNAC.Financier.Document
{
    //####################################################################################################
    //  Month / category class.
    //####################################################################################################

    public class MonthCategory : IDocument
    {
        //====================================================================================================
        //  Non-serializatble properties.
        //====================================================================================================

        [JsonIgnore]
        public BudgetRoot Root { get; set; }

        [JsonIgnore]
        public Category Category { get; set; }

        [JsonIgnore]
        public int Year { get; set; }

        [JsonIgnore]
        public int Month { get; set; }

        //====================================================================================================
        //  Serializable properties.
        //====================================================================================================

        public int budget { get; }
        public bool? overspending { get; }
        public string note { get; }
        public string _id { get { return $"b_{Root.Uuid}_m_category_{Year:D4}-{Month:D2}-01_{Category.Uuid}"; } }

        //====================================================================================================
        //  Constructor.
        //====================================================================================================

        public MonthCategory(BudgetRoot root, Category category, int budget, int year, int month, bool? overspending = null, string note = null)
        {
            Root = root;
            Category = category;
            Year = year;
            Month = month;

            this.budget = budget;
            this.overspending = overspending;
            this.note = note;
        }
    }
}
