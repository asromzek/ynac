﻿using Newtonsoft.Json;
using System;

namespace YNAC.Financier.Document
{
    //####################################################################################################
    //  Account class.
    //####################################################################################################

    public class AccountOffBudget : IAccount
    {
        //====================================================================================================
        //  Non-serializatble properties.
        //====================================================================================================

        [JsonIgnore]
        public FinancierBudget Budget { get; }

        [JsonIgnore]
        public string Uuid { get; }

        [JsonIgnore]
        public ACCOUNT_OFF_BUDGET Type { get; }

        //====================================================================================================
        //  Serializable properties.
        //====================================================================================================

        public string type { get; }
        public bool checkNumber { get; }
        public bool closed { get; }
        public string name { get; }
        public string note { get; }
        public int sort { get; set; }
        public bool onBudget { get; }
        public string _id { get { return $"b_{Budget.Root.Uuid}_account_{Uuid}"; } }

        //====================================================================================================
        //  Constructor.
        //====================================================================================================

        public AccountOffBudget(FinancierBudget budget, ACCOUNT_OFF_BUDGET type, string name, string note = null, bool checkNumber = false, bool closed = false)
        {
            Budget = budget;
            Type = type;
            Uuid = Guid.NewGuid().ToString();

            this.type = type.ToString();
            this.name = name;
            this.note = note;
            this.checkNumber = checkNumber;
            this.closed = closed;

            onBudget = false;
        }

        //====================================================================================================
        //  Add off budget normal transaction.
        //====================================================================================================

        public TransactionOffBudget AddTransaction(Payee payee, DateTime date, int value, string memo, bool cleared, bool reconciled, FLAG flag)
        {
            var Transaction = new TransactionOffBudget(Budget, this, payee, null, CATEGORY_TYPE.NORMAL, date, value, memo, cleared, reconciled, flag);
            Budget.Transactions.Add(Transaction);
            return Transaction;
        }

        //====================================================================================================
        //  Add transfer between off budget accounts.
        //====================================================================================================

        public Tuple<TransactionOffBudget, TransactionOffBudget> AddTransfer(bool srcCleared, bool srcReconciled, FLAG srcFlag, AccountOffBudget dstAccount, bool dstCleared, bool dstReconciled, FLAG dstFlag, DateTime date, int value, string memo)
        {
            var SourceUuid = Guid.NewGuid().ToString();
            var DestinationUuid = Guid.NewGuid().ToString();

            var SourceTransaction = new TransactionOffBudget(Budget, this, null, null, CATEGORY_TYPE.NORMAL, date, value, memo, srcCleared, srcReconciled, srcFlag, SourceUuid, DestinationUuid);
            var DestinationTransaction = new TransactionOffBudget(Budget, dstAccount, null, null, CATEGORY_TYPE.NORMAL, date, -value, memo, dstCleared, dstReconciled, dstFlag, DestinationUuid, SourceUuid);

            Budget.Transactions.Add(SourceTransaction);
            Budget.Transactions.Add(DestinationTransaction);

            return Tuple.Create(SourceTransaction, DestinationTransaction);
        }

        //====================================================================================================
        //  Add transfer from off budget to on budget account, normal transaction.
        //====================================================================================================

        public Tuple<TransactionOffBudget, TransactionOnBudget> AddTransferToOnBudget(bool srcCleared, bool srcReconciled, FLAG srcFlag, AccountOnBudget dstAccount, Category dstCategory, bool dstCleared, bool dstReconciled, FLAG dstFlag, DateTime date, int value, string memo)
        {
            var SourceUuid = Guid.NewGuid().ToString();
            var DestinationUuid = Guid.NewGuid().ToString();

            var SourceTransaction = new TransactionOffBudget(Budget, this, null, null, CATEGORY_TYPE.NORMAL, date, value, memo, srcCleared, srcReconciled, srcFlag, SourceUuid, DestinationUuid);
            var DestinationTransaction = new TransactionOnBudget(Budget, dstAccount, null, dstCategory, CATEGORY_TYPE.NORMAL, date, -value, memo, dstCleared, dstReconciled, dstFlag, DestinationUuid, SourceUuid);

            Budget.Transactions.Add(SourceTransaction);
            Budget.Transactions.Add(DestinationTransaction);

            return Tuple.Create(SourceTransaction, DestinationTransaction);
        }

        //====================================================================================================
        //  Add transfer from off budget to on budget account, income.
        //====================================================================================================

        public Tuple<TransactionOffBudget, TransactionOnBudget> AddTransferToOnBudgetIncome(bool srcCleared, bool srcReconciled, FLAG srcFlag, AccountOnBudget dstAccount, bool dstCleared, bool dstReconciled, FLAG dstFlag, DateTime date, int value, string memo)
        {
            var SourceUuid = Guid.NewGuid().ToString();
            var DestinationUuid = Guid.NewGuid().ToString();

            var SourceTransaction = new TransactionOffBudget(Budget, this, null, null, CATEGORY_TYPE.NORMAL, date, value, memo, srcCleared, srcReconciled, srcFlag, SourceUuid, DestinationUuid);
            var DestinationTransaction = new TransactionOnBudget(Budget, dstAccount, null, null, CATEGORY_TYPE.INCOME, date, -value, memo, dstCleared, dstReconciled, dstFlag, DestinationUuid, SourceUuid);

            Budget.Transactions.Add(SourceTransaction);
            Budget.Transactions.Add(DestinationTransaction);

            return Tuple.Create(SourceTransaction, DestinationTransaction);
        }

        //====================================================================================================
        //  Add transfer from off budget to on budget account, income for next month.
        //====================================================================================================

        public Tuple<TransactionOffBudget, TransactionOnBudget> AddTransferToOnBudgetIncomeNextMonth(bool srcCleared, bool srcReconciled, FLAG srcFlag, AccountOnBudget dstAccount, bool dstCleared, bool dstReconciled, FLAG dstFlag, DateTime date, int value, string memo)
        {
            var SourceUuid = Guid.NewGuid().ToString();
            var DestinationUuid = Guid.NewGuid().ToString();

            var SourceTransaction = new TransactionOffBudget(Budget, this, null, null, CATEGORY_TYPE.NORMAL, date, value, memo, srcCleared, srcReconciled, srcFlag, SourceUuid, DestinationUuid);
            var DestinationTransaction = new TransactionOnBudget(Budget, dstAccount, null, null, CATEGORY_TYPE.INCOME_NEXT_MONTH, date, -value, memo, dstCleared, dstReconciled, dstFlag, DestinationUuid, SourceUuid);

            Budget.Transactions.Add(SourceTransaction);
            Budget.Transactions.Add(DestinationTransaction);

            return Tuple.Create(SourceTransaction, DestinationTransaction);
        }
    }
}
