﻿using Newtonsoft.Json;
using System;

namespace YNAC.Financier.Document
{
    //####################################################################################################
    //  Account class.
    //####################################################################################################

    public class AccountOnBudget : IAccount
    {
        //====================================================================================================
        //  Non-serializatble properties.
        //====================================================================================================

        [JsonIgnore]
        public FinancierBudget Budget { get; }

        [JsonIgnore]
        public string Uuid { get; }

        [JsonIgnore]
        public ACCOUNT_ON_BUDGET Type { get; }

        //====================================================================================================
        //  Serializable properties.
        //====================================================================================================

        public string type { get; }
        public bool checkNumber { get; }
        public bool closed { get; }
        public string name { get; }
        public string note { get; }
        public int sort { get; set; }
        public bool onBudget { get; }
        public string _id { get { return $"b_{Budget.Root.Uuid}_account_{Uuid}"; } }

        //====================================================================================================
        //  Constructor.
        //====================================================================================================

        public AccountOnBudget(FinancierBudget budget, ACCOUNT_ON_BUDGET type, string name, string note = null, bool checkNumber = false, bool closed = false)
        {
            Budget = budget;
            Type = type;
            Uuid = Guid.NewGuid().ToString();

            this.type = type.ToString();
            this.name = name;
            this.note = note;
            this.checkNumber = checkNumber;
            this.closed = closed;
            onBudget = true;
        }

        //====================================================================================================
        //  Add on budget normal transaction.
        //====================================================================================================

        public TransactionOnBudget AddTransaction(Payee payee, Category category, DateTime date, int value, string memo, bool cleared, bool reconciled, FLAG flag)
        {
            var Transaction = new TransactionOnBudget(Budget, this, payee, category, CATEGORY_TYPE.NORMAL, date, value, memo, cleared, reconciled, flag);
            Budget.Transactions.Add(Transaction);
            return Transaction;
        }

        //====================================================================================================
        //  Add income transaction.
        //====================================================================================================

        public TransactionOnBudget AddIncome(Payee payee, DateTime date, int value, string memo, bool cleared, bool reconciled, FLAG flag)
        {
            var Transaction = new TransactionOnBudget(Budget, this, payee, null, CATEGORY_TYPE.INCOME, date, value, memo, cleared, reconciled, flag);
            Budget.Transactions.Add(Transaction);
            return Transaction;
        }

        //====================================================================================================
        //  Add income transaction.
        //====================================================================================================

        public TransactionOnBudget AddIncomeNextMonth(Payee payee, DateTime date, int value, string memo, bool cleared, bool reconciled, FLAG flag)
        {
            var Transaction = new TransactionOnBudget(Budget, this, payee, null, CATEGORY_TYPE.INCOME_NEXT_MONTH, date, value, memo, cleared, reconciled, flag);
            Budget.Transactions.Add(Transaction);
            return Transaction;
        }

        //====================================================================================================
        //  Add on budget normal transaction.
        //====================================================================================================

        public TransactionOnBudget AddSplitTransaction(Payee payee, DateTime date, string memo, bool cleared, bool reconciled, FLAG flag)
        {
            var Transaction = new TransactionOnBudget(Budget, this, payee, null, CATEGORY_TYPE.SPLIT, date, 0, memo, cleared, reconciled, flag);
            Budget.Transactions.Add(Transaction);
            return Transaction;
        }

        //====================================================================================================
        //  Add transfer between on budget accounts.
        //====================================================================================================

        public Tuple<TransactionOnBudget, TransactionOnBudget> AddTransfer(bool srcCleared, bool srcReconciled, FLAG srcFlag, AccountOnBudget dstAccount, bool dstCleared, bool dstReconciled, FLAG dstFlag, DateTime date, int value, string memo)
        {
            var SourceUuid = Guid.NewGuid().ToString();
            var DestinationUuid = Guid.NewGuid().ToString();

            var SourceTransaction = new TransactionOnBudget(Budget, this, null, null, CATEGORY_TYPE.NORMAL, date, value, memo, srcCleared, srcReconciled, srcFlag, SourceUuid, DestinationUuid);
            var DestinationTransaction = new TransactionOnBudget(Budget, dstAccount, null, null, CATEGORY_TYPE.NORMAL, date, -value, memo, dstCleared, dstReconciled, dstFlag, DestinationUuid, SourceUuid);

            Budget.Transactions.Add(SourceTransaction);
            Budget.Transactions.Add(DestinationTransaction);

            return Tuple.Create(SourceTransaction, DestinationTransaction);
        }

        //====================================================================================================
        //  Add transfer from on budget to off budget accounts.
        //====================================================================================================

        public Tuple<TransactionOnBudget, TransactionOffBudget> AddTransferToOffBudget(Category srcCategory, bool srcCleared, bool srcReconciled, FLAG srcFlag, AccountOffBudget dstAccount, bool dstCleared, bool dstReconciled, FLAG dstFlag, DateTime date, int value, string memo)
        {
            var SourceUuid = Guid.NewGuid().ToString();
            var DestinationUuid = Guid.NewGuid().ToString();

            var SourceTransaction = new TransactionOnBudget(Budget, this, null, srcCategory, CATEGORY_TYPE.NORMAL, date, value, memo, srcCleared, srcReconciled, srcFlag, SourceUuid, DestinationUuid);
            var DestinationTransaction = new TransactionOffBudget(Budget, dstAccount, null, null, CATEGORY_TYPE.NORMAL, date, -value, memo, dstCleared, dstReconciled, dstFlag, DestinationUuid, SourceUuid);

            Budget.Transactions.Add(SourceTransaction);
            Budget.Transactions.Add(DestinationTransaction);

            return Tuple.Create(SourceTransaction, DestinationTransaction);
        }
    }
}
