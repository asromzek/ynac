﻿using Newtonsoft.Json;
using System;

namespace YNAC.Financier.Document
{
    //####################################################################################################
    //  Category class.
    //####################################################################################################

    public class Category : IDocument
    {
        //====================================================================================================
        //  Non-serializatble properties.
        //====================================================================================================

        [JsonIgnore]
        public BudgetRoot Root { get; set; }

        [JsonIgnore]
        public MasterCategory Master { get; set; }

        [JsonIgnore]
        public string Uuid { get; set; }

        //====================================================================================================
        //  Serializable properties.
        //====================================================================================================

        public string name { get; }
        public string masterCategory { get { return Master.Uuid; } }
        public int sort { get; set; }
        public string _id { get { return $"b_{Root.Uuid}_category_{Uuid}"; } }

        //====================================================================================================
        //  Constructor.
        //====================================================================================================

        public Category(BudgetRoot root, MasterCategory master, string name)
        {
            Root = root;
            Master = master;
            Uuid = Guid.NewGuid().ToString();

            this.name = name;
        }
    }
}
