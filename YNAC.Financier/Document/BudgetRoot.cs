﻿using Newtonsoft.Json;
using System;

namespace YNAC.Financier.Document
{
    //####################################################################################################
    //  Budget root class.
    //####################################################################################################

    public class BudgetRoot : IDocument
    {
        //====================================================================================================
        //  Non-serializatble properties.
        //====================================================================================================

        [JsonIgnore]
        public FinancierBudget Budget { get; }

        [JsonIgnore]
        public string Uuid { get; }

        [JsonIgnore]
        public CURRENCY Currency { get; }

        //====================================================================================================
        //  Serializable properties.
        //====================================================================================================

        public BudgetHints hints { get; }
        public string name { get; }
        public string currency { get; }
        public string created { get; }
        public bool checkNumber { get; }
        public string _id { get { return $"budget_{Uuid}"; } }

        //====================================================================================================
        //  Constructor.
        //====================================================================================================

        public BudgetRoot(FinancierBudget budget, string name, CURRENCY currency, DateTime created)
        {
            Uuid = Guid.NewGuid().ToString();
            Budget = budget;
            Currency = currency;
            hints = new BudgetHints();

            this.name = name;
            this.currency = currency.ToString();
            this.created = created.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.fffZ");

            checkNumber = false;
        }
    }

    //####################################################################################################
    //  Budget hints class.
    //####################################################################################################

    public class BudgetHints
    {
        //====================================================================================================
        //  Serializable properties.
        //====================================================================================================

        public bool outflow { get; set; }

        //====================================================================================================
        //  Constructor.
        //====================================================================================================

        public BudgetHints()
        {
            outflow = true;
        }
    }
}
