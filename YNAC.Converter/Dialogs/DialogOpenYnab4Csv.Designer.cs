﻿namespace YNAC.YNABConverter.Dialogs
{
    partial class DialogOpenYnab4Csv
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelBudgetFile = new System.Windows.Forms.Label();
            this.LabelRegisterFile = new System.Windows.Forms.Label();
            this.TextBudgetFile = new System.Windows.Forms.TextBox();
            this.TextRegisterFile = new System.Windows.Forms.TextBox();
            this.ButtonBrowseRegister = new System.Windows.Forms.Button();
            this.ButtonBrowseBudget = new System.Windows.Forms.Button();
            this.ButtonCancel = new System.Windows.Forms.Button();
            this.ButtonOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LabelBudgetFile
            // 
            this.LabelBudgetFile.AutoSize = true;
            this.LabelBudgetFile.Location = new System.Drawing.Point(12, 15);
            this.LabelBudgetFile.Name = "LabelBudgetFile";
            this.LabelBudgetFile.Size = new System.Drawing.Size(60, 13);
            this.LabelBudgetFile.TabIndex = 0;
            this.LabelBudgetFile.Text = "Budget File";
            // 
            // LabelRegisterFile
            // 
            this.LabelRegisterFile.AutoSize = true;
            this.LabelRegisterFile.Location = new System.Drawing.Point(12, 41);
            this.LabelRegisterFile.Name = "LabelRegisterFile";
            this.LabelRegisterFile.Size = new System.Drawing.Size(65, 13);
            this.LabelRegisterFile.TabIndex = 1;
            this.LabelRegisterFile.Text = "Register File";
            // 
            // TextBudgetFile
            // 
            this.TextBudgetFile.Location = new System.Drawing.Point(83, 12);
            this.TextBudgetFile.Name = "TextBudgetFile";
            this.TextBudgetFile.Size = new System.Drawing.Size(400, 20);
            this.TextBudgetFile.TabIndex = 2;
            // 
            // TextRegisterFile
            // 
            this.TextRegisterFile.Location = new System.Drawing.Point(83, 38);
            this.TextRegisterFile.Name = "TextRegisterFile";
            this.TextRegisterFile.Size = new System.Drawing.Size(400, 20);
            this.TextRegisterFile.TabIndex = 3;
            // 
            // ButtonBrowseRegister
            // 
            this.ButtonBrowseRegister.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonBrowseRegister.Location = new System.Drawing.Point(489, 37);
            this.ButtonBrowseRegister.Name = "ButtonBrowseRegister";
            this.ButtonBrowseRegister.Size = new System.Drawing.Size(64, 20);
            this.ButtonBrowseRegister.TabIndex = 4;
            this.ButtonBrowseRegister.Text = "Browse";
            this.ButtonBrowseRegister.UseVisualStyleBackColor = true;
            // 
            // ButtonBrowseBudget
            // 
            this.ButtonBrowseBudget.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonBrowseBudget.Location = new System.Drawing.Point(489, 11);
            this.ButtonBrowseBudget.Name = "ButtonBrowseBudget";
            this.ButtonBrowseBudget.Size = new System.Drawing.Size(64, 20);
            this.ButtonBrowseBudget.TabIndex = 4;
            this.ButtonBrowseBudget.Text = "Browse";
            this.ButtonBrowseBudget.UseVisualStyleBackColor = true;
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ButtonCancel.Location = new System.Drawing.Point(453, 63);
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.Size = new System.Drawing.Size(100, 23);
            this.ButtonCancel.TabIndex = 5;
            this.ButtonCancel.Text = "Cancel";
            this.ButtonCancel.UseVisualStyleBackColor = true;
            // 
            // ButtonOK
            // 
            this.ButtonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.ButtonOK.Location = new System.Drawing.Point(347, 63);
            this.ButtonOK.Name = "ButtonOK";
            this.ButtonOK.Size = new System.Drawing.Size(100, 23);
            this.ButtonOK.TabIndex = 5;
            this.ButtonOK.Text = "OK";
            this.ButtonOK.UseVisualStyleBackColor = true;
            // 
            // DialogOpenYnab4Csv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(565, 98);
            this.Controls.Add(this.ButtonOK);
            this.Controls.Add(this.ButtonCancel);
            this.Controls.Add(this.ButtonBrowseBudget);
            this.Controls.Add(this.ButtonBrowseRegister);
            this.Controls.Add(this.TextRegisterFile);
            this.Controls.Add(this.TextBudgetFile);
            this.Controls.Add(this.LabelRegisterFile);
            this.Controls.Add(this.LabelBudgetFile);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DialogOpenYnab4Csv";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Open YNAB 4 CSV";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelBudgetFile;
        private System.Windows.Forms.Label LabelRegisterFile;
        private System.Windows.Forms.TextBox TextBudgetFile;
        private System.Windows.Forms.TextBox TextRegisterFile;
        private System.Windows.Forms.Button ButtonBrowseRegister;
        private System.Windows.Forms.Button ButtonBrowseBudget;
        private System.Windows.Forms.Button ButtonCancel;
        private System.Windows.Forms.Button ButtonOK;
    }
}