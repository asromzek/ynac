﻿using System;
using System.IO;
using System.Windows.Forms;

namespace YNAC.YNABConverter.Dialogs
{
    public partial class DialogOpenYnabOnlineCsv : Form
    {
        //====================================================================================================
        //  Public properties.
        //====================================================================================================

        public string BudgetZipPath
        {
            get { return TextBudgetZipPath.Text; }
            set { TextBudgetZipPath.Text = value; }
        }

        //====================================================================================================
        //  Constructor.
        //====================================================================================================

        public DialogOpenYnabOnlineCsv()
        {
            InitializeComponent();
        }

        //====================================================================================================
        //  Browse button click event handler.
        //====================================================================================================

        private void ButtonBrowseBudgetZip_Click(object sender, EventArgs e)
        {
            var DialogOpen = new OpenFileDialog();
            DialogOpen.InitialDirectory = "C:\\";
            DialogOpen.Filter = "ZIP Files (*.zip)|*.zip";

            var OpenResult = DialogOpen.ShowDialog();

            if (OpenResult == DialogResult.OK)
                BudgetZipPath = DialogOpen.FileName;
        }

        //====================================================================================================
        //  OK button click event handler.
        //====================================================================================================

        private void ButtonOK_Click(object sender, EventArgs e)
        {
            if (File.Exists(TextBudgetZipPath.Text))
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }
    }
}
