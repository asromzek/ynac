﻿namespace YNAC.YNABConverter.Dialogs
{
    partial class DialogOpenYnabOnlineCsv
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelBudgetFile = new System.Windows.Forms.Label();
            this.TextBudgetZipPath = new System.Windows.Forms.TextBox();
            this.ButtonBrowseBudgetZip = new System.Windows.Forms.Button();
            this.ButtonCancel = new System.Windows.Forms.Button();
            this.ButtonOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LabelBudgetFile
            // 
            this.LabelBudgetFile.AutoSize = true;
            this.LabelBudgetFile.Location = new System.Drawing.Point(12, 15);
            this.LabelBudgetFile.Name = "LabelBudgetFile";
            this.LabelBudgetFile.Size = new System.Drawing.Size(60, 13);
            this.LabelBudgetFile.TabIndex = 0;
            this.LabelBudgetFile.Text = "Budget File";
            // 
            // TextBudgetZipPath
            // 
            this.TextBudgetZipPath.BackColor = System.Drawing.Color.White;
            this.TextBudgetZipPath.Location = new System.Drawing.Point(83, 12);
            this.TextBudgetZipPath.Name = "TextBudgetZipPath";
            this.TextBudgetZipPath.ReadOnly = true;
            this.TextBudgetZipPath.Size = new System.Drawing.Size(400, 20);
            this.TextBudgetZipPath.TabIndex = 2;
            // 
            // ButtonBrowseBudgetZip
            // 
            this.ButtonBrowseBudgetZip.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonBrowseBudgetZip.Location = new System.Drawing.Point(489, 11);
            this.ButtonBrowseBudgetZip.Name = "ButtonBrowseBudgetZip";
            this.ButtonBrowseBudgetZip.Size = new System.Drawing.Size(64, 20);
            this.ButtonBrowseBudgetZip.TabIndex = 4;
            this.ButtonBrowseBudgetZip.Text = "Browse";
            this.ButtonBrowseBudgetZip.UseVisualStyleBackColor = true;
            this.ButtonBrowseBudgetZip.Click += new System.EventHandler(this.ButtonBrowseBudgetZip_Click);
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ButtonCancel.Location = new System.Drawing.Point(453, 38);
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.Size = new System.Drawing.Size(100, 23);
            this.ButtonCancel.TabIndex = 5;
            this.ButtonCancel.Text = "Cancel";
            this.ButtonCancel.UseVisualStyleBackColor = true;
            // 
            // ButtonOK
            // 
            this.ButtonOK.Location = new System.Drawing.Point(347, 38);
            this.ButtonOK.Name = "ButtonOK";
            this.ButtonOK.Size = new System.Drawing.Size(100, 23);
            this.ButtonOK.TabIndex = 5;
            this.ButtonOK.Text = "OK";
            this.ButtonOK.UseVisualStyleBackColor = true;
            this.ButtonOK.Click += new System.EventHandler(this.ButtonOK_Click);
            // 
            // DialogOpenYnabOnlineCsv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(565, 72);
            this.Controls.Add(this.ButtonOK);
            this.Controls.Add(this.ButtonCancel);
            this.Controls.Add(this.ButtonBrowseBudgetZip);
            this.Controls.Add(this.TextBudgetZipPath);
            this.Controls.Add(this.LabelBudgetFile);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DialogOpenYnabOnlineCsv";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Open YNAB Online CSV";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelBudgetFile;
        private System.Windows.Forms.TextBox TextBudgetZipPath;
        private System.Windows.Forms.Button ButtonBrowseBudgetZip;
        private System.Windows.Forms.Button ButtonCancel;
        private System.Windows.Forms.Button ButtonOK;
    }
}