﻿namespace YNAC.YNABConverter
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MenuMainStrip = new System.Windows.Forms.MenuStrip();
            this.MenuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuImportYnabOnlineCsv = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuImportYnab4Native = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuImportYnab4Csv = new System.Windows.Forms.ToolStripMenuItem();
            this.SeparatorFile1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuClearBudgetInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.SeparatorFile2 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuExport = new System.Windows.Forms.ToolStripMenuItem();
            this.SeparatorFile3 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.GroupBudgetInfo = new System.Windows.Forms.GroupBox();
            this.ComboCurrency = new System.Windows.Forms.ComboBox();
            this.TextBudgetName = new System.Windows.Forms.TextBox();
            this.LabelCurrency = new System.Windows.Forms.Label();
            this.LabelBudgetName = new System.Windows.Forms.Label();
            this.ListOnBudgetAccounts = new System.Windows.Forms.ListView();
            this.ColumnOnBudgetAccount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnTransactionCount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnBalance = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnOnBudgetOpen = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TabControlBudget = new System.Windows.Forms.TabControl();
            this.TabAccounts = new System.Windows.Forms.TabPage();
            this.TableAccountLayout = new System.Windows.Forms.TableLayoutPanel();
            this.ListOffBudgetAccounts = new System.Windows.Forms.ListView();
            this.ColumnOffBudgetAccount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnOffBudgetType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnOffBudgetTransactionCount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnOffBudgetBalance = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnOffBudgetOpen = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TabCategories = new System.Windows.Forms.TabPage();
            this.TreeCategories = new System.Windows.Forms.TreeView();
            this.ContextOnBudgetAccount = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MenuContextChecking = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuContextSavings = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuContextCreditCard = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuContextCash = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuContextLineOfCredit = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuContextMerchant = new System.Windows.Forms.ToolStripMenuItem();
            this.SeparatorOnBudget1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuContextOnBudgetOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuContextOnBudgetClosed = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextOffBudgetAccount = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MenuContextOffBudgetOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuContextOffBudgetClosed = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuContextMortgage = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuContextInvestment = new System.Windows.Forms.ToolStripMenuItem();
            this.SeparatorOffBudget1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuContextOtherAsset = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuContextOtherLiability = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMainStrip.SuspendLayout();
            this.GroupBudgetInfo.SuspendLayout();
            this.TabControlBudget.SuspendLayout();
            this.TabAccounts.SuspendLayout();
            this.TableAccountLayout.SuspendLayout();
            this.TabCategories.SuspendLayout();
            this.ContextOnBudgetAccount.SuspendLayout();
            this.ContextOffBudgetAccount.SuspendLayout();
            this.SuspendLayout();
            // 
            // MenuMainStrip
            // 
            this.MenuMainStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuFile,
            this.MenuHelp});
            this.MenuMainStrip.Location = new System.Drawing.Point(0, 0);
            this.MenuMainStrip.Name = "MenuMainStrip";
            this.MenuMainStrip.Size = new System.Drawing.Size(838, 24);
            this.MenuMainStrip.TabIndex = 0;
            this.MenuMainStrip.Text = "menuStrip1";
            // 
            // MenuFile
            // 
            this.MenuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuImportYnabOnlineCsv,
            this.MenuImportYnab4Native,
            this.MenuImportYnab4Csv,
            this.SeparatorFile1,
            this.MenuClearBudgetInfo,
            this.SeparatorFile2,
            this.MenuExport,
            this.SeparatorFile3,
            this.MenuExit});
            this.MenuFile.Name = "MenuFile";
            this.MenuFile.Size = new System.Drawing.Size(37, 20);
            this.MenuFile.Text = "&File";
            // 
            // MenuImportYnabOnlineCsv
            // 
            this.MenuImportYnabOnlineCsv.Name = "MenuImportYnabOnlineCsv";
            this.MenuImportYnabOnlineCsv.Size = new System.Drawing.Size(231, 22);
            this.MenuImportYnabOnlineCsv.Text = "Import YNAB Online CSV";
            this.MenuImportYnabOnlineCsv.Click += new System.EventHandler(this.MenuImportYnabOnlineCsv_Click);
            // 
            // MenuImportYnab4Native
            // 
            this.MenuImportYnab4Native.Enabled = false;
            this.MenuImportYnab4Native.Name = "MenuImportYnab4Native";
            this.MenuImportYnab4Native.Size = new System.Drawing.Size(231, 22);
            this.MenuImportYnab4Native.Text = "Import YNAB 4 Native Budget";
            this.MenuImportYnab4Native.Click += new System.EventHandler(this.MenuImportYnab4Native_Click);
            // 
            // MenuImportYnab4Csv
            // 
            this.MenuImportYnab4Csv.Enabled = false;
            this.MenuImportYnab4Csv.Name = "MenuImportYnab4Csv";
            this.MenuImportYnab4Csv.Size = new System.Drawing.Size(231, 22);
            this.MenuImportYnab4Csv.Text = "Import YNAB 4 CSV";
            this.MenuImportYnab4Csv.Click += new System.EventHandler(this.MenuImportYnab4Csv_Click);
            // 
            // SeparatorFile1
            // 
            this.SeparatorFile1.Name = "SeparatorFile1";
            this.SeparatorFile1.Size = new System.Drawing.Size(228, 6);
            // 
            // MenuClearBudgetInfo
            // 
            this.MenuClearBudgetInfo.Name = "MenuClearBudgetInfo";
            this.MenuClearBudgetInfo.Size = new System.Drawing.Size(231, 22);
            this.MenuClearBudgetInfo.Text = "&Clear Budget Information";
            this.MenuClearBudgetInfo.Click += new System.EventHandler(this.MenuClearBudgetInfo_Click);
            // 
            // SeparatorFile2
            // 
            this.SeparatorFile2.Name = "SeparatorFile2";
            this.SeparatorFile2.Size = new System.Drawing.Size(228, 6);
            // 
            // MenuExport
            // 
            this.MenuExport.Name = "MenuExport";
            this.MenuExport.Size = new System.Drawing.Size(231, 22);
            this.MenuExport.Text = "Export to Financier JSON";
            this.MenuExport.Click += new System.EventHandler(this.MenuExport_Click);
            // 
            // SeparatorFile3
            // 
            this.SeparatorFile3.Name = "SeparatorFile3";
            this.SeparatorFile3.Size = new System.Drawing.Size(228, 6);
            // 
            // MenuExit
            // 
            this.MenuExit.Name = "MenuExit";
            this.MenuExit.Size = new System.Drawing.Size(231, 22);
            this.MenuExit.Text = "E&xit";
            this.MenuExit.Click += new System.EventHandler(this.MenuExit_Click);
            // 
            // MenuHelp
            // 
            this.MenuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuAbout});
            this.MenuHelp.Name = "MenuHelp";
            this.MenuHelp.Size = new System.Drawing.Size(44, 20);
            this.MenuHelp.Text = "&Help";
            // 
            // MenuAbout
            // 
            this.MenuAbout.Name = "MenuAbout";
            this.MenuAbout.Size = new System.Drawing.Size(107, 22);
            this.MenuAbout.Text = "&About";
            // 
            // GroupBudgetInfo
            // 
            this.GroupBudgetInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBudgetInfo.Controls.Add(this.ComboCurrency);
            this.GroupBudgetInfo.Controls.Add(this.TextBudgetName);
            this.GroupBudgetInfo.Controls.Add(this.LabelCurrency);
            this.GroupBudgetInfo.Controls.Add(this.LabelBudgetName);
            this.GroupBudgetInfo.Location = new System.Drawing.Point(12, 27);
            this.GroupBudgetInfo.Name = "GroupBudgetInfo";
            this.GroupBudgetInfo.Size = new System.Drawing.Size(814, 76);
            this.GroupBudgetInfo.TabIndex = 1;
            this.GroupBudgetInfo.TabStop = false;
            this.GroupBudgetInfo.Text = "Budget Settings";
            // 
            // ComboCurrency
            // 
            this.ComboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboCurrency.FormattingEnabled = true;
            this.ComboCurrency.Location = new System.Drawing.Point(61, 45);
            this.ComboCurrency.Name = "ComboCurrency";
            this.ComboCurrency.Size = new System.Drawing.Size(121, 21);
            this.ComboCurrency.TabIndex = 2;
            // 
            // TextBudgetName
            // 
            this.TextBudgetName.Location = new System.Drawing.Point(61, 19);
            this.TextBudgetName.Name = "TextBudgetName";
            this.TextBudgetName.Size = new System.Drawing.Size(300, 20);
            this.TextBudgetName.TabIndex = 1;
            // 
            // LabelCurrency
            // 
            this.LabelCurrency.AutoSize = true;
            this.LabelCurrency.Location = new System.Drawing.Point(6, 48);
            this.LabelCurrency.Name = "LabelCurrency";
            this.LabelCurrency.Size = new System.Drawing.Size(49, 13);
            this.LabelCurrency.TabIndex = 0;
            this.LabelCurrency.Text = "Currency";
            // 
            // LabelBudgetName
            // 
            this.LabelBudgetName.AutoSize = true;
            this.LabelBudgetName.Location = new System.Drawing.Point(6, 22);
            this.LabelBudgetName.Name = "LabelBudgetName";
            this.LabelBudgetName.Size = new System.Drawing.Size(35, 13);
            this.LabelBudgetName.TabIndex = 0;
            this.LabelBudgetName.Text = "Name";
            // 
            // ListOnBudgetAccounts
            // 
            this.ListOnBudgetAccounts.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnOnBudgetAccount,
            this.ColumnType,
            this.ColumnTransactionCount,
            this.ColumnBalance,
            this.ColumnOnBudgetOpen});
            this.ListOnBudgetAccounts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListOnBudgetAccounts.FullRowSelect = true;
            this.ListOnBudgetAccounts.GridLines = true;
            this.ListOnBudgetAccounts.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.ListOnBudgetAccounts.Location = new System.Drawing.Point(3, 3);
            this.ListOnBudgetAccounts.Name = "ListOnBudgetAccounts";
            this.ListOnBudgetAccounts.Size = new System.Drawing.Size(794, 185);
            this.ListOnBudgetAccounts.TabIndex = 0;
            this.ListOnBudgetAccounts.UseCompatibleStateImageBehavior = false;
            this.ListOnBudgetAccounts.View = System.Windows.Forms.View.Details;
            this.ListOnBudgetAccounts.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ListOnBudgetAccounts_MouseClick);
            // 
            // ColumnOnBudgetAccount
            // 
            this.ColumnOnBudgetAccount.Text = "On Budget Account";
            this.ColumnOnBudgetAccount.Width = 150;
            // 
            // ColumnType
            // 
            this.ColumnType.Text = "Type";
            this.ColumnType.Width = 150;
            // 
            // ColumnTransactionCount
            // 
            this.ColumnTransactionCount.Text = "Transaction Count";
            this.ColumnTransactionCount.Width = 100;
            // 
            // ColumnBalance
            // 
            this.ColumnBalance.Text = "Balance";
            this.ColumnBalance.Width = 100;
            // 
            // ColumnOnBudgetOpen
            // 
            this.ColumnOnBudgetOpen.Text = "Open/Closed";
            this.ColumnOnBudgetOpen.Width = 100;
            // 
            // TabControlBudget
            // 
            this.TabControlBudget.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabControlBudget.Controls.Add(this.TabAccounts);
            this.TabControlBudget.Controls.Add(this.TabCategories);
            this.TabControlBudget.Location = new System.Drawing.Point(12, 109);
            this.TabControlBudget.Name = "TabControlBudget";
            this.TabControlBudget.SelectedIndex = 0;
            this.TabControlBudget.Size = new System.Drawing.Size(814, 414);
            this.TabControlBudget.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.TabControlBudget.TabIndex = 4;
            // 
            // TabAccounts
            // 
            this.TabAccounts.Controls.Add(this.TableAccountLayout);
            this.TabAccounts.Location = new System.Drawing.Point(4, 22);
            this.TabAccounts.Name = "TabAccounts";
            this.TabAccounts.Padding = new System.Windows.Forms.Padding(3);
            this.TabAccounts.Size = new System.Drawing.Size(806, 388);
            this.TabAccounts.TabIndex = 0;
            this.TabAccounts.Text = "Accounts";
            this.TabAccounts.UseVisualStyleBackColor = true;
            // 
            // TableAccountLayout
            // 
            this.TableAccountLayout.ColumnCount = 1;
            this.TableAccountLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TableAccountLayout.Controls.Add(this.ListOnBudgetAccounts, 0, 0);
            this.TableAccountLayout.Controls.Add(this.ListOffBudgetAccounts, 0, 1);
            this.TableAccountLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableAccountLayout.Location = new System.Drawing.Point(3, 3);
            this.TableAccountLayout.Name = "TableAccountLayout";
            this.TableAccountLayout.RowCount = 2;
            this.TableAccountLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableAccountLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableAccountLayout.Size = new System.Drawing.Size(800, 382);
            this.TableAccountLayout.TabIndex = 0;
            // 
            // ListOffBudgetAccounts
            // 
            this.ListOffBudgetAccounts.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnOffBudgetAccount,
            this.ColumnOffBudgetType,
            this.ColumnOffBudgetTransactionCount,
            this.ColumnOffBudgetBalance,
            this.ColumnOffBudgetOpen});
            this.ListOffBudgetAccounts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListOffBudgetAccounts.FullRowSelect = true;
            this.ListOffBudgetAccounts.GridLines = true;
            this.ListOffBudgetAccounts.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.ListOffBudgetAccounts.Location = new System.Drawing.Point(3, 194);
            this.ListOffBudgetAccounts.Name = "ListOffBudgetAccounts";
            this.ListOffBudgetAccounts.Size = new System.Drawing.Size(794, 185);
            this.ListOffBudgetAccounts.TabIndex = 1;
            this.ListOffBudgetAccounts.UseCompatibleStateImageBehavior = false;
            this.ListOffBudgetAccounts.View = System.Windows.Forms.View.Details;
            this.ListOffBudgetAccounts.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ListOffBudgetAccounts_MouseClick);
            // 
            // ColumnOffBudgetAccount
            // 
            this.ColumnOffBudgetAccount.Text = "Off Budget Account";
            this.ColumnOffBudgetAccount.Width = 150;
            // 
            // ColumnOffBudgetType
            // 
            this.ColumnOffBudgetType.Text = "Type";
            this.ColumnOffBudgetType.Width = 150;
            // 
            // ColumnOffBudgetTransactionCount
            // 
            this.ColumnOffBudgetTransactionCount.Text = "Transaction Count";
            this.ColumnOffBudgetTransactionCount.Width = 100;
            // 
            // ColumnOffBudgetBalance
            // 
            this.ColumnOffBudgetBalance.Text = "Balance";
            this.ColumnOffBudgetBalance.Width = 100;
            // 
            // ColumnOffBudgetOpen
            // 
            this.ColumnOffBudgetOpen.Text = "Open/Closed";
            this.ColumnOffBudgetOpen.Width = 100;
            // 
            // TabCategories
            // 
            this.TabCategories.Controls.Add(this.TreeCategories);
            this.TabCategories.Location = new System.Drawing.Point(4, 22);
            this.TabCategories.Name = "TabCategories";
            this.TabCategories.Padding = new System.Windows.Forms.Padding(3);
            this.TabCategories.Size = new System.Drawing.Size(806, 388);
            this.TabCategories.TabIndex = 1;
            this.TabCategories.Text = "Categories";
            this.TabCategories.UseVisualStyleBackColor = true;
            // 
            // TreeCategories
            // 
            this.TreeCategories.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TreeCategories.Location = new System.Drawing.Point(6, 6);
            this.TreeCategories.Name = "TreeCategories";
            this.TreeCategories.Size = new System.Drawing.Size(794, 334);
            this.TreeCategories.TabIndex = 0;
            // 
            // ContextOnBudgetAccount
            // 
            this.ContextOnBudgetAccount.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuContextChecking,
            this.MenuContextSavings,
            this.MenuContextCreditCard,
            this.MenuContextCash,
            this.MenuContextLineOfCredit,
            this.MenuContextMerchant,
            this.SeparatorOnBudget1,
            this.MenuContextOnBudgetOpen,
            this.MenuContextOnBudgetClosed});
            this.ContextOnBudgetAccount.Name = "ContextOnBudgetAccount";
            this.ContextOnBudgetAccount.Size = new System.Drawing.Size(174, 186);
            // 
            // MenuContextChecking
            // 
            this.MenuContextChecking.Name = "MenuContextChecking";
            this.MenuContextChecking.Size = new System.Drawing.Size(173, 22);
            this.MenuContextChecking.Text = "C&hecking";
            this.MenuContextChecking.Click += new System.EventHandler(this.MenuContextChecking_Click);
            // 
            // MenuContextSavings
            // 
            this.MenuContextSavings.Name = "MenuContextSavings";
            this.MenuContextSavings.Size = new System.Drawing.Size(173, 22);
            this.MenuContextSavings.Text = "&Savings";
            this.MenuContextSavings.Click += new System.EventHandler(this.MenuContextSavings_Click);
            // 
            // MenuContextCreditCard
            // 
            this.MenuContextCreditCard.Name = "MenuContextCreditCard";
            this.MenuContextCreditCard.Size = new System.Drawing.Size(173, 22);
            this.MenuContextCreditCard.Text = "C&redit Card";
            this.MenuContextCreditCard.Click += new System.EventHandler(this.MenuContextCreditCard_Click);
            // 
            // MenuContextCash
            // 
            this.MenuContextCash.Name = "MenuContextCash";
            this.MenuContextCash.Size = new System.Drawing.Size(173, 22);
            this.MenuContextCash.Text = "C&ash";
            this.MenuContextCash.Click += new System.EventHandler(this.MenuContextCash_Click);
            // 
            // MenuContextLineOfCredit
            // 
            this.MenuContextLineOfCredit.Name = "MenuContextLineOfCredit";
            this.MenuContextLineOfCredit.Size = new System.Drawing.Size(173, 22);
            this.MenuContextLineOfCredit.Text = "&Line of Credit";
            this.MenuContextLineOfCredit.Click += new System.EventHandler(this.MenuContextLineOfCredit_Click);
            // 
            // MenuContextMerchant
            // 
            this.MenuContextMerchant.Name = "MenuContextMerchant";
            this.MenuContextMerchant.Size = new System.Drawing.Size(173, 22);
            this.MenuContextMerchant.Text = "&Merchant Account";
            this.MenuContextMerchant.Click += new System.EventHandler(this.MenuContextMerchant_Click);
            // 
            // SeparatorOnBudget1
            // 
            this.SeparatorOnBudget1.Name = "SeparatorOnBudget1";
            this.SeparatorOnBudget1.Size = new System.Drawing.Size(170, 6);
            // 
            // MenuContextOnBudgetOpen
            // 
            this.MenuContextOnBudgetOpen.Name = "MenuContextOnBudgetOpen";
            this.MenuContextOnBudgetOpen.Size = new System.Drawing.Size(173, 22);
            this.MenuContextOnBudgetOpen.Text = "&Open";
            this.MenuContextOnBudgetOpen.Click += new System.EventHandler(this.MenuContextOnBudgetOpen_Click);
            // 
            // MenuContextOnBudgetClosed
            // 
            this.MenuContextOnBudgetClosed.Name = "MenuContextOnBudgetClosed";
            this.MenuContextOnBudgetClosed.Size = new System.Drawing.Size(173, 22);
            this.MenuContextOnBudgetClosed.Text = "&Closed";
            this.MenuContextOnBudgetClosed.Click += new System.EventHandler(this.MenuContextOnBudgetClosed_Click);
            // 
            // ContextOffBudgetAccount
            // 
            this.ContextOffBudgetAccount.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuContextMortgage,
            this.MenuContextInvestment,
            this.MenuContextOtherAsset,
            this.MenuContextOtherLiability,
            this.SeparatorOffBudget1,
            this.MenuContextOffBudgetOpen,
            this.MenuContextOffBudgetClosed});
            this.ContextOffBudgetAccount.Name = "ContextOnBudgetAccount";
            this.ContextOffBudgetAccount.Size = new System.Drawing.Size(182, 164);
            // 
            // MenuContextOffBudgetOpen
            // 
            this.MenuContextOffBudgetOpen.Name = "MenuContextOffBudgetOpen";
            this.MenuContextOffBudgetOpen.Size = new System.Drawing.Size(181, 22);
            this.MenuContextOffBudgetOpen.Text = "&Open";
            this.MenuContextOffBudgetOpen.Click += new System.EventHandler(this.MenuOffBudgetOpen_Click);
            // 
            // MenuContextOffBudgetClosed
            // 
            this.MenuContextOffBudgetClosed.Name = "MenuContextOffBudgetClosed";
            this.MenuContextOffBudgetClosed.Size = new System.Drawing.Size(181, 22);
            this.MenuContextOffBudgetClosed.Text = "&Closed";
            this.MenuContextOffBudgetClosed.Click += new System.EventHandler(this.MenuContextOffBudgetClosed_Click);
            // 
            // MenuContextMortgage
            // 
            this.MenuContextMortgage.Name = "MenuContextMortgage";
            this.MenuContextMortgage.Size = new System.Drawing.Size(181, 22);
            this.MenuContextMortgage.Text = "&Mortgage";
            this.MenuContextMortgage.Click += new System.EventHandler(this.MenuContextMortgage_Click);
            // 
            // MenuContextInvestment
            // 
            this.MenuContextInvestment.Name = "MenuContextInvestment";
            this.MenuContextInvestment.Size = new System.Drawing.Size(181, 22);
            this.MenuContextInvestment.Text = "&Investment Account";
            this.MenuContextInvestment.Click += new System.EventHandler(this.MenuContextInvestment_Click);
            // 
            // SeparatorOffBudget1
            // 
            this.SeparatorOffBudget1.Name = "SeparatorOffBudget1";
            this.SeparatorOffBudget1.Size = new System.Drawing.Size(178, 6);
            // 
            // MenuContextOtherAsset
            // 
            this.MenuContextOtherAsset.Name = "MenuContextOtherAsset";
            this.MenuContextOtherAsset.Size = new System.Drawing.Size(181, 22);
            this.MenuContextOtherAsset.Text = "Other &Asset";
            this.MenuContextOtherAsset.Click += new System.EventHandler(this.MenuContextOtherAsset_Click);
            // 
            // MenuContextOtherLiability
            // 
            this.MenuContextOtherLiability.Name = "MenuContextOtherLiability";
            this.MenuContextOtherLiability.Size = new System.Drawing.Size(181, 22);
            this.MenuContextOtherLiability.Text = "Other &Liability";
            this.MenuContextOtherLiability.Click += new System.EventHandler(this.MenuContextOtherLiability_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(838, 535);
            this.Controls.Add(this.TabControlBudget);
            this.Controls.Add(this.GroupBudgetInfo);
            this.Controls.Add(this.MenuMainStrip);
            this.MainMenuStrip = this.MenuMainStrip;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "YNAC";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.MenuMainStrip.ResumeLayout(false);
            this.MenuMainStrip.PerformLayout();
            this.GroupBudgetInfo.ResumeLayout(false);
            this.GroupBudgetInfo.PerformLayout();
            this.TabControlBudget.ResumeLayout(false);
            this.TabAccounts.ResumeLayout(false);
            this.TableAccountLayout.ResumeLayout(false);
            this.TabCategories.ResumeLayout(false);
            this.ContextOnBudgetAccount.ResumeLayout(false);
            this.ContextOffBudgetAccount.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MenuMainStrip;
        private System.Windows.Forms.ToolStripMenuItem MenuFile;
        private System.Windows.Forms.ToolStripMenuItem MenuImportYnab4Csv;
        private System.Windows.Forms.ToolStripMenuItem MenuImportYnabOnlineCsv;
        private System.Windows.Forms.ToolStripSeparator SeparatorFile2;
        private System.Windows.Forms.ToolStripMenuItem MenuExit;
        private System.Windows.Forms.ToolStripMenuItem MenuImportYnab4Native;
        private System.Windows.Forms.ToolStripMenuItem MenuHelp;
        private System.Windows.Forms.ToolStripMenuItem MenuAbout;
        private System.Windows.Forms.ToolStripSeparator SeparatorFile1;
        private System.Windows.Forms.GroupBox GroupBudgetInfo;
        private System.Windows.Forms.ComboBox ComboCurrency;
        private System.Windows.Forms.TextBox TextBudgetName;
        private System.Windows.Forms.Label LabelCurrency;
        private System.Windows.Forms.Label LabelBudgetName;
        private System.Windows.Forms.ListView ListOnBudgetAccounts;
        private System.Windows.Forms.ColumnHeader ColumnOnBudgetAccount;
        private System.Windows.Forms.ColumnHeader ColumnType;
        private System.Windows.Forms.ColumnHeader ColumnTransactionCount;
        private System.Windows.Forms.ColumnHeader ColumnBalance;
        private System.Windows.Forms.TabControl TabControlBudget;
        private System.Windows.Forms.TabPage TabAccounts;
        private System.Windows.Forms.TableLayoutPanel TableAccountLayout;
        private System.Windows.Forms.TabPage TabCategories;
        private System.Windows.Forms.ListView ListOffBudgetAccounts;
        private System.Windows.Forms.ColumnHeader ColumnOffBudgetAccount;
        private System.Windows.Forms.ColumnHeader ColumnOffBudgetType;
        private System.Windows.Forms.ColumnHeader ColumnOffBudgetTransactionCount;
        private System.Windows.Forms.ColumnHeader ColumnOffBudgetBalance;
        private System.Windows.Forms.TreeView TreeCategories;
        private System.Windows.Forms.ColumnHeader ColumnOnBudgetOpen;
        private System.Windows.Forms.ColumnHeader ColumnOffBudgetOpen;
        private System.Windows.Forms.ToolStripMenuItem MenuExport;
        private System.Windows.Forms.ToolStripSeparator SeparatorFile3;
        private System.Windows.Forms.ToolStripMenuItem MenuClearBudgetInfo;
        private System.Windows.Forms.ContextMenuStrip ContextOnBudgetAccount;
        private System.Windows.Forms.ContextMenuStrip ContextOffBudgetAccount;
        private System.Windows.Forms.ToolStripMenuItem MenuContextChecking;
        private System.Windows.Forms.ToolStripMenuItem MenuContextSavings;
        private System.Windows.Forms.ToolStripMenuItem MenuContextCreditCard;
        private System.Windows.Forms.ToolStripMenuItem MenuContextCash;
        private System.Windows.Forms.ToolStripMenuItem MenuContextLineOfCredit;
        private System.Windows.Forms.ToolStripMenuItem MenuContextMerchant;
        private System.Windows.Forms.ToolStripSeparator SeparatorOnBudget1;
        private System.Windows.Forms.ToolStripMenuItem MenuContextOnBudgetOpen;
        private System.Windows.Forms.ToolStripMenuItem MenuContextOnBudgetClosed;
        private System.Windows.Forms.ToolStripMenuItem MenuContextMortgage;
        private System.Windows.Forms.ToolStripMenuItem MenuContextInvestment;
        private System.Windows.Forms.ToolStripMenuItem MenuContextOtherAsset;
        private System.Windows.Forms.ToolStripMenuItem MenuContextOtherLiability;
        private System.Windows.Forms.ToolStripSeparator SeparatorOffBudget1;
        private System.Windows.Forms.ToolStripMenuItem MenuContextOffBudgetOpen;
        private System.Windows.Forms.ToolStripMenuItem MenuContextOffBudgetClosed;
    }
}

