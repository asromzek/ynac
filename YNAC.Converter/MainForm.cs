﻿using Csv;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using YNAC.Converter.Budget;
using YNAC.Financier;
using YNAC.YNABConverter.Dialogs;

namespace YNAC.YNABConverter
{
    public partial class MainForm : Form
    {
        //====================================================================================================
        //  Private variables.
        //====================================================================================================

        private Budget Budget;

        //====================================================================================================
        //  Private property variables.
        //====================================================================================================

        private bool _BudgetOpen;

        //====================================================================================================
        //  Public properties.
        //====================================================================================================

        public bool BudgetOpen
        {
            get { return _BudgetOpen; }
            set
            {
                _BudgetOpen = value;

                MenuExport.Enabled = _BudgetOpen;
                MenuImportYnabOnlineCsv.Enabled = !_BudgetOpen;
                MenuClearBudgetInfo.Enabled = _BudgetOpen;

                GroupBudgetInfo.Enabled = _BudgetOpen;
                TabControlBudget.Enabled = _BudgetOpen;
            }
        }

        //====================================================================================================
        //  Constructor.
        //====================================================================================================

        public MainForm()
        {
            InitializeComponent();

            ComboCurrency.DataSource = Enum.GetValues(typeof(CURRENCY));
            BudgetOpen = false;
        }

        //====================================================================================================
        //  Import native YNAB 4 budget menu click event handler.
        //====================================================================================================

        private void MenuImportYnab4Native_Click(object sender, EventArgs e)
        {
            // TODO: This will be fun...
        }

        //====================================================================================================
        //  Import exported YNAB 4 CSV menu click event handler.
        //====================================================================================================

        private void MenuImportYnab4Csv_Click(object sender, EventArgs e)
        {
            // TODO: Piece of cake compared to YNAB online.
        }

        //====================================================================================================
        //  Import exported YNAB Online CSV menu click event handler.
        //====================================================================================================

        private void MenuImportYnabOnlineCsv_Click(object sender, EventArgs e)
        {
            //----------------------------------------------------------------------------------------------------
            //  Display the open file dialog and get the result.
            //----------------------------------------------------------------------------------------------------

            var OpenDialog = new DialogOpenYnabOnlineCsv();
            var Result = OpenDialog.ShowDialog();

            //----------------------------------------------------------------------------------------------------
            //  Cancel if the OK button wasn't clicked.
            //----------------------------------------------------------------------------------------------------

            if (Result != DialogResult.OK)
                return;

            //----------------------------------------------------------------------------------------------------
            //  Validate the selected filename to see if it matches the YNAB Online export filename format.
            //----------------------------------------------------------------------------------------------------

            if (!Path.GetFileName(OpenDialog.BudgetZipPath).StartsWith("YNAB Export - ") || !Path.GetFileName(OpenDialog.BudgetZipPath).EndsWith(".zip"))
                return;

            //----------------------------------------------------------------------------------------------------
            //  Extract the budget and register CSV files from the zip file, and load into memory.
            //----------------------------------------------------------------------------------------------------

            var BudgetName = Path.GetFileName(OpenDialog.BudgetZipPath).Replace("YNAB Export - ", "").Replace(".zip", "");
            var Zip = ZipFile.Read(OpenDialog.BudgetZipPath);

            var ZipBudget = Zip[BudgetName + " - Budget.csv"];
            var ZipRegister = Zip[BudgetName + " - Register.csv"];

            ZipBudget.Extract(Path.GetTempPath(), ExtractExistingFileAction.OverwriteSilently);
            ZipRegister.Extract(Path.GetTempPath(), ExtractExistingFileAction.OverwriteSilently);

            string BudgetData;
            string RegisterData;

            using (var Reader = new StreamReader(Path.Combine(Path.GetTempPath(), BudgetName + " - Budget.csv")))
                BudgetData = Reader.ReadToEnd();

            using (var Reader = new StreamReader(Path.Combine(Path.GetTempPath(), BudgetName + " - Register.csv")))
                RegisterData = Reader.ReadToEnd();

            //----------------------------------------------------------------------------------------------------
            //  Parse the budget and register CSV files into some IEnumerable lists.
            //----------------------------------------------------------------------------------------------------

            var BudgetLines = CsvReader.ReadFromText(BudgetData);
            var RegisterLines = CsvReader.ReadFromText(RegisterData);

            //----------------------------------------------------------------------------------------------------
            //  Parse YNAB Online CSV file and populated the budget structre with data.
            //----------------------------------------------------------------------------------------------------

            Budget = new Budget(BudgetName);
            ParseYnabOnlineCsv(BudgetLines, RegisterLines);

            //----------------------------------------------------------------------------------------------------
            //  Set the budget name. TODO: Auto detect the currency in the CSV file.
            //----------------------------------------------------------------------------------------------------

            TextBudgetName.Text = BudgetName;
        }

        //====================================================================================================
        //  Exit menu click event handler.
        //====================================================================================================

        private void MenuExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //====================================================================================================
        //  Parse the YNAB Online CSV file format and build a budget structure.
        //====================================================================================================

        private void ParseYnabOnlineCsv(IEnumerable<ICsvLine> BudgetLines, IEnumerable<ICsvLine> RegisterLines)
        {
            var OnBudgetIndex = 0;
            var OffBudgetIndex = 0;

            var RegisterLineCount = Enumerable.Count(RegisterLines);
            var BudgetLineCount = Enumerable.Count(BudgetLines);

            var SplitMode = false;
            var CurrentSplit = new Transaction();
            var SplitMemoPattern = new Regex(@"Split \((?<SplitNumber>\d+)/(?<SplitTotal>\d+)\) (?<Memo>.*)");

            //----------------------------------------------------------------------------------------------------
            //  Create master category and category lists from Budget file to retain budget structure. Also,
            //  generate monthly budget amounts.
            //----------------------------------------------------------------------------------------------------

            foreach (var Line in BudgetLines)
            {
                var LineMonth = Line["Month"].Substring(0, 3);
                var LineYear = Line["Month"].Substring(4, 4);

                int Month = 1;
                int Year = Convert.ToInt32(LineYear);

                if (LineMonth == "Jan")
                    Month = 1;
                if (LineMonth == "Feb")
                    Month = 2;
                if (LineMonth == "Mar")
                    Month = 3;
                if (LineMonth == "Apr")
                    Month = 4;
                if (LineMonth == "May")
                    Month = 5;
                if (LineMonth == "Jun")
                    Month = 6;
                if (LineMonth == "Jul")
                    Month = 7;
                if (LineMonth == "Aug")
                    Month = 8;
                if (LineMonth == "Sep")
                    Month = 9;
                if (LineMonth == "Oct")
                    Month = 10;
                if (LineMonth == "Nov")
                    Month = 11;
                if (LineMonth == "Dec")
                    Month = 12;

                MasterCategory MasterCategory;

                if (!Budget.MasterCategories.ContainsKey(Line["Category Group"]))
                {
                    MasterCategory = new MasterCategory(Line["Category Group"]);
                    Budget.MasterCategories.Add(Line["Category Group"], MasterCategory);
                }
                else
                    MasterCategory = Budget.MasterCategories[Line["Category Group"]];

                if (!MasterCategory.Categories.ContainsKey(Line["Category"]))
                    MasterCategory.Categories.Add(Line["Category"], new Category(Line["Category"], MasterCategory));

                var Category = Budget.MasterCategories[Line["Category Group"]].Categories[Line["Category"]];
                Budget.MonthBudgets.Add(Line["Month"] + ":" + Line["Category Group/Category"], new MonthBudget(Year, Month, Category, Convert.ToInt32((Convert.ToDouble(Line["Budgeted"].Replace("$", "")) * 100))));
            }

            //----------------------------------------------------------------------------------------------------
            //  Create master category and category lists from Register file to fill in the blanks, mostly hidden.
            //----------------------------------------------------------------------------------------------------

            foreach (var Line in RegisterLines)
            {
                if ((Line["Category Group"] != string.Empty) && (Line["Category Group"] != "Income"))
                {
                    MasterCategory MasterCategory;

                    if (!Budget.MasterCategories.ContainsKey(Line["Category Group"]))
                    {
                        MasterCategory = new MasterCategory(Line["Category Group"]);
                        Budget.MasterCategories.Add(Line["Category Group"], MasterCategory);
                    }
                    else
                        MasterCategory = Budget.MasterCategories[Line["Category Group"]];

                    if (!MasterCategory.Categories.ContainsKey(Line["Category"]))
                        MasterCategory.Categories.Add(Line["Category"], new Category(Line["Category"], MasterCategory));
                }
            }

            //----------------------------------------------------------------------------------------------------
            //  Create list of payee.
            //----------------------------------------------------------------------------------------------------

            foreach (var Line in RegisterLines)
            {
                if (!Line["Payee"].StartsWith("Transfer : "))
                    if (!Budget.Payees.ContainsKey(Line["Payee"]))
                        Budget.Payees.Add(Line["Payee"], new Payee(Line["Payee"]));
            }

            //----------------------------------------------------------------------------------------------------
            //  Create list of accounts and update balances of existing accounts.
            //----------------------------------------------------------------------------------------------------

            foreach (var Line in RegisterLines)
            {
                //----------------------------------------------------------------------------------------------------
                //  Add the account if it does not exist.
                //----------------------------------------------------------------------------------------------------

                if (!Budget.Accounts.ContainsKey(Line["Account"]))
                    Budget.Accounts.Add(Line["Account"], new Account(Line["Account"]));

                //----------------------------------------------------------------------------------------------------
                //  Update account as ON BUDGET if a master category or category is found.
                //----------------------------------------------------------------------------------------------------

                if (Line["Category Group"] != string.Empty || Line["Category"] != string.Empty)
                {
                    Budget.Accounts[Line["Account"]].Type = "Savings";
                    Budget.Accounts[Line["Account"]].OnBudget = true;
                    Budget.Accounts[Line["Account"]].SortIndex = OnBudgetIndex;
                    OnBudgetIndex += 1;
                    OffBudgetIndex += 1;
                }
                else
                {
                    Budget.Accounts[Line["Account"]].Type = "Other Asset";
                    Budget.Accounts[Line["Account"]].OnBudget = false;
                    Budget.Accounts[Line["Account"]].SortIndex = OffBudgetIndex;
                    OffBudgetIndex += 1;
                }

                //----------------------------------------------------------------------------------------------------
                //  Update the account balance.
                //----------------------------------------------------------------------------------------------------

                Budget.Accounts[Line["Account"]].Balance += (Convert.ToInt32((Convert.ToDouble(Line["Inflow"].Replace("$", "")) * 100) - Convert.ToInt32((Convert.ToDouble(Line["Outflow"].Replace("$", "")) * 100))));
            }

            //----------------------------------------------------------------------------------------------------
            //  Create transactions list.
            //----------------------------------------------------------------------------------------------------

            foreach (var Line in RegisterLines)
            {
                var Match = SplitMemoPattern.Match(Line["Memo"]);

                if (!SplitMode)
                {
                    if (!Match.Success)
                    {
                        //----------------------------------------------------------------------------------------------------
                        //  Create a normal transaction if not currently processing a split, and "Split (X/Y)" is not found
                        //  in the memo.
                        //----------------------------------------------------------------------------------------------------

                        var NewTransaction = CreateTransaction(Line, false);
                        Budget.Accounts[Line["Account"]].Transactions.Add(NewTransaction);
                    }
                    else
                    {
                        //----------------------------------------------------------------------------------------------------
                        //  Since we're not in split mode yet, and a new split was detected, create a new split transaction.
                        //----------------------------------------------------------------------------------------------------

                        var NewTransaction = CreateTransaction(Line, true);
                        Budget.Accounts[Line["Account"]].Transactions.Add(NewTransaction);

                        //----------------------------------------------------------------------------------------------------
                        //  Create the first sub transaction for the split.
                        //----------------------------------------------------------------------------------------------------

                        var NewSubTransaction = new SubTransaction();
                        NewTransaction.SubTransactions.Add(NewSubTransaction);

                        NewSubTransaction.IsIncome = (Line["Category Group"] == "Inflow");

                        if (Line["Payee"] != string.Empty)
                            NewSubTransaction.Payee = Budget.Payees[Line["Payee"]];

                        if ((Line["Category Group"] != string.Empty) && (Line["Category"] != string.Empty) && (Line["Category Group"] != "Inflow"))
                        {
                            NewSubTransaction.MasterCategory = Budget.MasterCategories[Line["Category Group"]];
                            NewSubTransaction.Category = NewSubTransaction.MasterCategory.Categories[Line["Category"]];
                        }

                        NewSubTransaction.Outflow = Convert.ToInt32(Convert.ToDouble(Line["Outflow"].Replace("$", "")) * 100);
                        NewSubTransaction.Inflow = Convert.ToInt32(Convert.ToDouble(Line["Inflow"].Replace("$", "")) * 100);

                        //----------------------------------------------------------------------------------------------------
                        //  Set split mode and the current transaction.
                        //----------------------------------------------------------------------------------------------------

                        SplitMode = true;
                        CurrentSplit = NewTransaction;
                    }
                }
                else
                {
                    //----------------------------------------------------------------------------------------------------
                    //  Since we're in split mode, and "Split (X/Y)" was found in the memo, add another sub transaction
                    //  to the current split transaction.
                    //----------------------------------------------------------------------------------------------------

                    var NewSubTransaction = new SubTransaction();
                    CurrentSplit.SubTransactions.Add(NewSubTransaction);

                    NewSubTransaction.IsIncome = (Line["Category Group"] == "Inflow");

                    if (Line["Payee"] != string.Empty)
                        NewSubTransaction.Payee = Budget.Payees[Line["Payee"]];

                    if ((Line["Category Group"] != string.Empty) && (Line["Category"] != string.Empty) && (Line["Category Group"] != "Inflow"))
                    {
                        NewSubTransaction.MasterCategory = Budget.MasterCategories[Line["Category Group"]];
                        NewSubTransaction.Category = NewSubTransaction.MasterCategory.Categories[Line["Category"]];
                    }

                    NewSubTransaction.Outflow = Convert.ToInt32(Convert.ToDouble(Line["Outflow"].Replace("$", "")) * 100);
                    NewSubTransaction.Inflow = Convert.ToInt32(Convert.ToDouble(Line["Inflow"].Replace("$", "")) * 100);

                    //----------------------------------------------------------------------------------------------------
                    //  Reset split mode if we have reached the last split subtransaction. When X = Y in "Split (X/Y)".
                    //----------------------------------------------------------------------------------------------------

                    if (Match.Groups["SplitNumber"].Value == Match.Groups["SplitTotal"].Value)
                    {
                        SplitMode = false;
                        CurrentSplit = null;
                    }
                }
            }

            //----------------------------------------------------------------------------------------------------
            //  The budget import is complete. Update the UI.
            //----------------------------------------------------------------------------------------------------

            BudgetOpen = true;
            UpdateUI();
        }

        //====================================================================================================
        //  Update UI with budget information.
        //====================================================================================================

        private void UpdateUI()
        {
            //----------------------------------------------------------------------------------------------------
            //  Clear any existing data from the controls on the form.
            //----------------------------------------------------------------------------------------------------

            ListOnBudgetAccounts.Items.Clear();
            ListOffBudgetAccounts.Items.Clear();
            TreeCategories.Nodes.Clear();

            //----------------------------------------------------------------------------------------------------
            //  Populate the on budget and off budget account listviews.
            //----------------------------------------------------------------------------------------------------

            foreach (var Account in Budget.Accounts.Values)
            {
                if (Account.OnBudget)
                {
                    var AccountItem = new ListViewItem(Account.Name);
                    AccountItem.SubItems.Add(Account.Type);
                    AccountItem.SubItems.Add(Account.Transactions.Count.ToString());
                    AccountItem.SubItems.Add($"{(Account.Balance / 100.0).ToString("F2")}");
                    AccountItem.SubItems.Add(Account.Closed ? "Closed" : "Open");

                    AccountItem.Tag = Account;
                    ListOnBudgetAccounts.Items.Add(AccountItem);
                }
                else
                {
                    var AccountItem = new ListViewItem(Account.Name);
                    AccountItem.SubItems.Add(Account.Type);
                    AccountItem.SubItems.Add(Account.Transactions.Count.ToString());
                    AccountItem.SubItems.Add($"{(Account.Balance / 100.0).ToString("F2")}");
                    AccountItem.SubItems.Add(Account.Closed ? "Closed" : "Open");

                    AccountItem.Tag = Account;
                    ListOffBudgetAccounts.Items.Add(AccountItem);
                }
            }
        }

        //====================================================================================================
        //  Create new transaction.
        //====================================================================================================

        private Transaction CreateTransaction(ICsvLine Line, bool isSplit)
        {
            var TransferPattern = new Regex(@"Transfer : (?<TransferAccount>.*)");
            var TransferMatch = TransferPattern.Match(Line["Payee"]);

            var NewTransaction = new Transaction();

            NewTransaction.IsSplit = isSplit;
            NewTransaction.IsIncome = (Line["Category Group"] == "Inflow");
            NewTransaction.IsTransfer = TransferMatch.Success;
            NewTransaction.Account = Budget.Accounts[Line["Account"]];
            NewTransaction.Date = DateTime.Parse(Line["Date"]);
            NewTransaction.Payee = (isSplit || NewTransaction.IsTransfer) ? null : Budget.Payees[Line["Payee"]];
            NewTransaction.Outflow = isSplit ? 0 : Convert.ToInt32(Convert.ToDouble(Line["Outflow"].Replace("$", "")) * 100);
            NewTransaction.Inflow = isSplit ? 0 : Convert.ToInt32(Convert.ToDouble(Line["Inflow"].Replace("$", "")) * 100);
            NewTransaction.Memo = Line["Memo"];
            NewTransaction.Flag = Line["Flag"];
            NewTransaction.TransferAccount = (NewTransaction.IsTransfer) ? Budget.Accounts[TransferMatch.Groups["TransferAccount"].Value] : null;

            if ((Line["Category Group"] != string.Empty) && (Line["Category"] != string.Empty) && !isSplit && (Line["Category Group"] != "Inflow"))
            {
                NewTransaction.MasterCategory = Budget.MasterCategories[Line["Category Group"]];
                NewTransaction.Category = NewTransaction.MasterCategory.Categories[Line["Category"]];
            }

            switch (Line["Cleared"])
            {
                case "Reconciled":
                    NewTransaction.Cleared = true;
                    NewTransaction.Reconciled = true;
                    break;

                case "Cleared":
                    NewTransaction.Cleared = true;
                    NewTransaction.Reconciled = false;
                    break;

                default:
                    NewTransaction.Cleared = false;
                    NewTransaction.Reconciled = false;
                    break;
            }

            return NewTransaction;
        }
        //====================================================================================================
        //  Clear budget info menu click event handler.
        //====================================================================================================

        private void MenuClearBudgetInfo_Click(object sender, EventArgs e)
        {
            Budget = null;

            TextBudgetName.Text = "";
            ComboCurrency.Text = "USD";
            ListOnBudgetAccounts.Items.Clear();
            ListOffBudgetAccounts.Items.Clear();

            BudgetOpen = false;
        }

        //====================================================================================================
        //  Export menu click event handler.
        //====================================================================================================

        private void MenuExport_Click(object sender, EventArgs e)
        {
            var BudgetBuilder = new FinancierBudget(TextBudgetName.Text, (CURRENCY)ComboCurrency.SelectedItem, DateTime.Now);

            //----------------------------------------------------------------------------------------------------
            //  Build the payee documents.
            //----------------------------------------------------------------------------------------------------

            foreach (var Payee in Budget.Payees)
                BudgetBuilder.AddPayee(Payee.Value.Name);

            //----------------------------------------------------------------------------------------------------
            //  Build the master category and category documents.
            //----------------------------------------------------------------------------------------------------

            foreach (var MasterCategory in Budget.MasterCategories)
            {
                var MasterCategoryDoc = BudgetBuilder.AddMasterCategory(MasterCategory.Value.Name);

                foreach (var Category in MasterCategory.Value.Categories)
                    BudgetBuilder.AddCategory(MasterCategoryDoc, Category.Value.Name);
            }

            //----------------------------------------------------------------------------------------------------
            //  Build the month-budget documents.
            //----------------------------------------------------------------------------------------------------

            foreach (var MonthBudget in Budget.MonthBudgets.Values)
            {
                //----------------------------------------------------------------------------------------------------
                //  Get the category for the month-budget.
                //----------------------------------------------------------------------------------------------------

                Financier.Document.Category CategoryDoc = null;
                string MasterCategoryID = "";

                foreach (var MasterCategory in BudgetBuilder.MasterCategories)
                    if (MasterCategory.name == MonthBudget.Category.MasterCategory.Name)
                        MasterCategoryID = MasterCategory._id.Substring(55, 36);

                foreach (var Category in BudgetBuilder.Categories)
                    if ((Category.masterCategory == MasterCategoryID) && (Category.name == MonthBudget.Category.Name))
                        CategoryDoc = Category;

                var Result = BudgetBuilder.AddMonthBudget(CategoryDoc, MonthBudget.Amount, MonthBudget.Year, MonthBudget.Month);
                int i = 0;
            }

            //----------------------------------------------------------------------------------------------------
            //  Create the account documents.
            //----------------------------------------------------------------------------------------------------

            foreach (var Account in Budget.Accounts.Values)
            {
                if (Account.OnBudget)
                {
                    //----------------------------------------------------------------------------------------------------
                    //  Create on budget account.
                    //----------------------------------------------------------------------------------------------------

                    var AccountType = ACCOUNT_ON_BUDGET.SAVINGS;

                    switch (Account.Type)
                    {
                        case "Checking":
                            AccountType = ACCOUNT_ON_BUDGET.DEBIT;
                            break;
                        case "Savings":
                            AccountType = ACCOUNT_ON_BUDGET.SAVINGS;
                            break;
                        case "Credit Card":
                            AccountType = ACCOUNT_ON_BUDGET.CREDIT;
                            break;
                        case "Cash":
                            AccountType = ACCOUNT_ON_BUDGET.CASH;
                            break;
                        case "Line of Credit":
                            AccountType = ACCOUNT_ON_BUDGET.OTHERCREDIT;
                            break;
                        case "Merchant Account":
                            AccountType = ACCOUNT_ON_BUDGET.MERCHANT;
                            break;
                    }

                    //----------------------------------------------------------------------------------------------------
                    //  Add on budget account document.
                    //----------------------------------------------------------------------------------------------------

                    BudgetBuilder.AddOnBudgetAccount(AccountType, Account.Name, null, false, Account.Closed);
                }
                else
                {
                    //----------------------------------------------------------------------------------------------------
                    //  Create off budget account.
                    //----------------------------------------------------------------------------------------------------

                    var AccountType = ACCOUNT_OFF_BUDGET.ASSET;

                    switch (Account.Type)
                    {
                        case "Mortgage":
                            AccountType = ACCOUNT_OFF_BUDGET.MORTGAGE;
                            break;
                        case "Investment Account":
                            AccountType = ACCOUNT_OFF_BUDGET.INVESTMENT;
                            break;
                        case "Other Asset":
                            AccountType = ACCOUNT_OFF_BUDGET.ASSET;
                            break;
                        case "Other Liability":
                            AccountType = ACCOUNT_OFF_BUDGET.LOAN;
                            break;
                    }

                    //----------------------------------------------------------------------------------------------------
                    //  Add off budget account document.
                    //----------------------------------------------------------------------------------------------------

                    BudgetBuilder.AddOffBudgetAccount(AccountType, Account.Name, null, false, Account.Closed);
                }
            }

            //----------------------------------------------------------------------------------------------------
            //  Create the on budget account documents.
            //----------------------------------------------------------------------------------------------------

            foreach (var Account in Budget.Accounts.Values)
            {
                if (Account.OnBudget)
                {
                    //----------------------------------------------------------------------------------------------------
                    //  Process transactions for the account.
                    //----------------------------------------------------------------------------------------------------

                    foreach (var Transaction in Account.Transactions)
                    {
                        //----------------------------------------------------------------------------------------------------
                        //  Get the account document for the transaction.
                        //----------------------------------------------------------------------------------------------------

                        Financier.Document.AccountOnBudget AccountDoc = null;

                        foreach (var AccountItem in BudgetBuilder.Accounts)
                            if (AccountItem.name == Transaction.Account.Name)
                                AccountDoc = (Financier.Document.AccountOnBudget)AccountItem;

                        //----------------------------------------------------------------------------------------------------
                        //  Get the payee document for the transaction.
                        //----------------------------------------------------------------------------------------------------

                        Financier.Document.Payee PayeeDoc = null;

                        if (Transaction.Payee != null)
                            foreach (var Payee in BudgetBuilder.Payees)
                                if (Payee.name == Transaction.Payee.Name)
                                    PayeeDoc = Payee;

                        //----------------------------------------------------------------------------------------------------
                        //  Get the category for the sub transaction.
                        //----------------------------------------------------------------------------------------------------

                        Financier.Document.Category CategoryDoc = null;
                        string MasterCategoryID = "";

                        if (Transaction.MasterCategory != null)
                        {
                            foreach (var MasterCategory in BudgetBuilder.MasterCategories)
                                if (MasterCategory.name == Transaction.MasterCategory.Name)
                                    MasterCategoryID = MasterCategory._id.Substring(55, 36);

                            foreach (var Category in BudgetBuilder.Categories)
                                if ((Category.masterCategory == MasterCategoryID) && (Category.name == Transaction.Category.Name))
                                    CategoryDoc = Category;
                        }

                        //----------------------------------------------------------------------------------------------------
                        //  Convert the flag string to correct enum value.
                        //----------------------------------------------------------------------------------------------------

                        var BudgetTransactionFlag = ConvertFlag(Transaction.Flag);

                        //----------------------------------------------------------------------------------------------------
                        //  Create the transaction documents.
                        //----------------------------------------------------------------------------------------------------

                        if (Transaction.IsSplit)
                        {
                            //----------------------------------------------------------------------------------------------------
                            //  Add a split transaction document.
                            //----------------------------------------------------------------------------------------------------

                            var TransactionDoc = AccountDoc.AddSplitTransaction(PayeeDoc, Transaction.Date, Transaction.Memo, Transaction.Cleared, Transaction.Reconciled, BudgetTransactionFlag);

                            //----------------------------------------------------------------------------------------------------
                            //  Process sub transactions.
                            //----------------------------------------------------------------------------------------------------

                            foreach (var SubTransaction in Transaction.SubTransactions)
                            {
                                //----------------------------------------------------------------------------------------------------
                                //  Get the payee document for the sub transaction.
                                //----------------------------------------------------------------------------------------------------

                                Financier.Document.Payee SubPayeeDoc = null;

                                if (SubTransaction.Payee != null)
                                    foreach (var Payee in BudgetBuilder.Payees)
                                        if (Payee.name == SubTransaction.Payee.Name)
                                            SubPayeeDoc = Payee;

                                //----------------------------------------------------------------------------------------------------
                                //  Get the category for the sub transaction.
                                //----------------------------------------------------------------------------------------------------

                                Financier.Document.Category SubCategoryDoc = null;
                                string SubMasterCategoryID = "";

                                if (SubTransaction.MasterCategory != null)
                                {
                                    foreach (var MasterCategory in BudgetBuilder.MasterCategories)
                                        if (MasterCategory.name == SubTransaction.MasterCategory.Name)
                                            SubMasterCategoryID = MasterCategory._id.Substring(55, 36);

                                    foreach (var Category in BudgetBuilder.Categories)
                                        if ((Category.masterCategory == SubMasterCategoryID) && (Category.name == SubTransaction.Category.Name))
                                            SubCategoryDoc = Category;
                                }

                                if (SubTransaction.IsIncome)
                                {
                                    //----------------------------------------------------------------------------------------------------
                                    //  Add the income sub transaction document.
                                    //----------------------------------------------------------------------------------------------------

                                    TransactionDoc.AddSplitIncome(SubTransaction.Inflow - SubTransaction.Outflow, SubPayeeDoc, SubTransaction.Memo);
                                }
                                else
                                {
                                    //----------------------------------------------------------------------------------------------------
                                    //  Add the sub transaction document.
                                    //----------------------------------------------------------------------------------------------------

                                    TransactionDoc.AddSplit(SubCategoryDoc, SubTransaction.Inflow - SubTransaction.Outflow, SubPayeeDoc, SubTransaction.Memo);
                                }
                            }
                        }
                        else if (Transaction.IsTransfer)
                        {
                            //----------------------------------------------------------------------------------------------------
                            //  Create transfer transaction document.
                            //----------------------------------------------------------------------------------------------------

                            var TransferAccount = Budget.Accounts[Transaction.TransferAccount.Name];
                            Transaction TransferTransaction = null;
                            var TransferCleared = false;
                            var TransferReconciled = false;
                            var TransferFlag = FLAG.NONE;

                            foreach (var TransactionItem in TransferAccount.Transactions)
                            {
                                if ((TransactionItem.TransferAccount == Account) && (TransactionItem.Inflow == Transaction.Outflow) && (TransactionItem.Outflow == Transaction.Inflow))
                                {
                                    TransferTransaction = TransactionItem;
                                    TransferCleared = TransactionItem.Cleared;
                                    TransferReconciled = TransactionItem.Reconciled;
                                    TransferFlag = ConvertFlag(TransactionItem.Flag);
                                }
                            }

                            if (TransferTransaction != null)
                                TransferAccount.Transactions.Remove(TransferTransaction);

                            Financier.Document.IAccount BudgetTransferAccount = null;

                            foreach (var AccountItem in BudgetBuilder.Accounts)
                                if (AccountItem.name == TransferAccount.Name)
                                    BudgetTransferAccount = AccountItem;
                            
                            var TransferType = BudgetTransferAccount.GetType();

                            if (TransferType == typeof(Financier.Document.AccountOnBudget))
                                AccountDoc.AddTransfer(Transaction.Cleared, Transaction.Reconciled, BudgetTransactionFlag, (Financier.Document.AccountOnBudget)BudgetTransferAccount, TransferCleared, TransferReconciled, TransferFlag, Transaction.Date, Transaction.Inflow - Transaction.Outflow, Transaction.Memo);
                            else
                                AccountDoc.AddTransferToOffBudget(CategoryDoc, Transaction.Cleared, Transaction.Reconciled, BudgetTransactionFlag, (Financier.Document.AccountOffBudget)BudgetTransferAccount, TransferCleared, TransferReconciled, TransferFlag, Transaction.Date, Transaction.Inflow - Transaction.Outflow, Transaction.Memo);
                        }
                        else if (Transaction.IsIncome)
                        {
                            //----------------------------------------------------------------------------------------------------
                            //  Create income transaction.
                            //----------------------------------------------------------------------------------------------------

                            AccountDoc.AddIncome(PayeeDoc, Transaction.Date, Transaction.Inflow - Transaction.Outflow, Transaction.Memo, Transaction.Cleared, Transaction.Reconciled, BudgetTransactionFlag);
                        }
                        else
                        {
                            //----------------------------------------------------------------------------------------------------
                            //  Create normal transaction document.
                            //----------------------------------------------------------------------------------------------------

                            AccountDoc.AddTransaction(PayeeDoc, CategoryDoc, Transaction.Date, Transaction.Inflow - Transaction.Outflow, Transaction.Memo, Transaction.Cleared, Transaction.Reconciled, BudgetTransactionFlag);
                        }
                    }
                }
            }

            //----------------------------------------------------------------------------------------------------
            //  Create the on budget account documents.
            //----------------------------------------------------------------------------------------------------

            foreach (var Account in Budget.Accounts.Values)
            {
                if (!Account.OnBudget)
                {
                    //----------------------------------------------------------------------------------------------------
                    //  Process transactions for the account.
                    //----------------------------------------------------------------------------------------------------

                    foreach (var Transaction in Account.Transactions)
                    {
                        //----------------------------------------------------------------------------------------------------
                        //  Get the account document for the transaction.
                        //----------------------------------------------------------------------------------------------------

                        Financier.Document.AccountOffBudget AccountDoc = null;

                        foreach (var AccountItem in BudgetBuilder.Accounts)
                            if (AccountItem.name == Transaction.Account.Name)
                                AccountDoc = (Financier.Document.AccountOffBudget)AccountItem;

                        //----------------------------------------------------------------------------------------------------
                        //  Get the payee document for the transaction.
                        //----------------------------------------------------------------------------------------------------

                        Financier.Document.Payee PayeeDoc = null;

                        if (Transaction.Payee != null)
                            foreach (var Payee in BudgetBuilder.Payees)
                                if (Payee.name == Transaction.Payee.Name)
                                    PayeeDoc = Payee;

                        //----------------------------------------------------------------------------------------------------
                        //  Convert the flag string to correct enum value.
                        //----------------------------------------------------------------------------------------------------

                        var BudgetTransactionFlag = ConvertFlag(Transaction.Flag);

                        //----------------------------------------------------------------------------------------------------
                        //  Create transaction document.
                        //----------------------------------------------------------------------------------------------------

                        if (Transaction.IsTransfer)
                        {
                            //----------------------------------------------------------------------------------------------------
                            //  Create transfer transaction document.
                            //----------------------------------------------------------------------------------------------------

                            var TransferAccount = Budget.Accounts[Transaction.TransferAccount.Name];
                            Transaction TransferTransaction = null;
                            var TransferCleared = false;
                            var TransferReconciled = false;
                            var TransferFlag = FLAG.NONE;

                            foreach (var TransactionItem in TransferAccount.Transactions)
                            {
                                if ((TransactionItem.TransferAccount == Account) && (TransactionItem.Inflow == Transaction.Outflow) && (TransactionItem.Outflow == Transaction.Inflow))
                                {
                                    TransferTransaction = TransactionItem;
                                    TransferCleared = TransactionItem.Cleared;
                                    TransferReconciled = TransactionItem.Reconciled;
                                    TransferFlag = ConvertFlag(TransactionItem.Flag);
                                }
                            }

                            if (TransferTransaction != null)
                                TransferAccount.Transactions.Remove(TransferTransaction);

                            Financier.Document.IAccount BudgetTransferAccount = null;

                            foreach (var AccountItem in BudgetBuilder.Accounts)
                                if (AccountItem.name == TransferAccount.Name)
                                    BudgetTransferAccount = AccountItem;

                            AccountDoc.AddTransfer(Transaction.Cleared, Transaction.Reconciled, BudgetTransactionFlag, (Financier.Document.AccountOffBudget)BudgetTransferAccount, TransferCleared, TransferReconciled, TransferFlag, Transaction.Date, Transaction.Inflow - Transaction.Outflow, Transaction.Memo);
                        }
                        else
                        {
                            //----------------------------------------------------------------------------------------------------
                            //  Create normal transaction document.
                            //----------------------------------------------------------------------------------------------------

                            AccountDoc.AddTransaction(PayeeDoc, Transaction.Date, Transaction.Inflow - Transaction.Outflow, Transaction.Memo, Transaction.Cleared, Transaction.Reconciled, BudgetTransactionFlag);
                        }
                    }
                }
            }

            //----------------------------------------------------------------------------------------------------
            //  Convert the budget structure to a JSON string.
            //----------------------------------------------------------------------------------------------------

            var BudgetJson = BudgetBuilder.BuildJson();

            //----------------------------------------------------------------------------------------------------
            //  Write the budget JSON to a file.
            //----------------------------------------------------------------------------------------------------

            using (var Writer = new StreamWriter(@"C:\ynac\budget.json"))
                Writer.Write(BudgetJson);

            //Console.WriteLine(BudgetJson);
        }

        //====================================================================================================
        //  Convert the flag string to correct enum value.
        //====================================================================================================

        private FLAG ConvertFlag(string flag)
        {
            switch (flag)
            {
                case "Red":
                    return FLAG.RED;
                case "Orange":
                    return FLAG.ORANGE;
                case "Yellow":
                    return FLAG.YELLOW;
                case "Green":
                    return FLAG.GREEN;
                case "Blue":
                    return FLAG.BLUE;
                case "Purple":
                    return FLAG.PURPLE;
                default:
                    return FLAG.NONE;
            }
        }

        //====================================================================================================
        //  On budget account list click event handler.
        //====================================================================================================

        private void ListOnBudgetAccounts_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (ListOnBudgetAccounts.FocusedItem.Bounds.Contains(e.Location))
                {
                    bool FirstIteration = true;
                    bool MultipleType = false;
                    bool MultipleClosed = false;

                    var TypeMemory = "";
                    var ClosedMemory = false;

                    //----------------------------------------------------------------------------------------------------
                    //  Check all of the selected accounts to see if they have the same type and are all open/closed.
                    //----------------------------------------------------------------------------------------------------

                    foreach (ListViewItem Item in ListOnBudgetAccounts.SelectedItems)
                    {
                        if (FirstIteration)
                        {
                            TypeMemory = ((Account)Item.Tag).Type;
                            ClosedMemory = ((Account)Item.Tag).Closed;
                        }
                        else
                        {
                            if (TypeMemory != ((Account)Item.Tag).Type)
                                MultipleType = true;

                            if (ClosedMemory != ((Account)Item.Tag).Closed)
                                MultipleClosed = true;
                        }

                        FirstIteration = false;
                    }

                    //----------------------------------------------------------------------------------------------------
                    //  Set the correct account type checkbox if the selected account(s) is/are of the same type. Don't
                    //  set any checkboxes if accounts of multiple types are selected.
                    //----------------------------------------------------------------------------------------------------

                    if (MultipleType)
                    {
                        MenuContextChecking.Checked = false;
                        MenuContextSavings.Checked = false;
                        MenuContextCreditCard.Checked = false;
                        MenuContextCash.Checked = false;
                        MenuContextLineOfCredit.Checked = false;
                        MenuContextMerchant.Checked = false;
                    }
                    else
                    {
                        MenuContextChecking.Checked = ((Account)ListOnBudgetAccounts.SelectedItems[0].Tag).Type == "Checking";
                        MenuContextSavings.Checked = ((Account)ListOnBudgetAccounts.SelectedItems[0].Tag).Type == "Savings";
                        MenuContextCreditCard.Checked = ((Account)ListOnBudgetAccounts.SelectedItems[0].Tag).Type == "Credit Card";
                        MenuContextCash.Checked = ((Account)ListOnBudgetAccounts.SelectedItems[0].Tag).Type == "Cash";
                        MenuContextLineOfCredit.Checked = ((Account)ListOnBudgetAccounts.SelectedItems[0].Tag).Type == "Line of Credit";
                        MenuContextMerchant.Checked = ((Account)ListOnBudgetAccounts.SelectedItems[0].Tag).Type == "Merchant Account";
                    }

                    //----------------------------------------------------------------------------------------------------
                    //  Set the correct account open/closed checkbox if the selected account(s) is/are all open or closed.
                    //  Don't set any checkboxes if aa mixture of open/closed accounts are selected.
                    //----------------------------------------------------------------------------------------------------

                    if (MultipleClosed)
                    {
                        MenuContextOnBudgetOpen.Checked = false;
                        MenuContextOnBudgetClosed.Checked = false;
                    }
                    else
                    {
                        MenuContextOnBudgetOpen.Checked = !((Account)ListOnBudgetAccounts.SelectedItems[0].Tag).Closed;
                        MenuContextOnBudgetClosed.Checked = ((Account)ListOnBudgetAccounts.SelectedItems[0].Tag).Closed;
                    }

                    //----------------------------------------------------------------------------------------------------
                    //  Display the context menu.
                    //----------------------------------------------------------------------------------------------------

                    ContextOnBudgetAccount.Show(Cursor.Position);
                }
            }
        }

        //====================================================================================================
        //  On budget account context menu click event handlers.
        //====================================================================================================

        private void MenuContextChecking_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem Item in ListOnBudgetAccounts.SelectedItems)
                ((Account)Item.Tag).Type = "Checking";

            UpdateUI();
        }

        private void MenuContextSavings_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem Item in ListOnBudgetAccounts.SelectedItems)
                ((Account)Item.Tag).Type = "Savings";

            UpdateUI();
        }

        private void MenuContextCreditCard_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem Item in ListOnBudgetAccounts.SelectedItems)
                ((Account)Item.Tag).Type = "Credit Card";

            UpdateUI();
        }

        private void MenuContextCash_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem Item in ListOnBudgetAccounts.SelectedItems)
                ((Account)Item.Tag).Type = "Cash";

            UpdateUI();
        }

        private void MenuContextLineOfCredit_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem Item in ListOnBudgetAccounts.SelectedItems)
                ((Account)Item.Tag).Type = "Line of Credit";

            UpdateUI();
        }

        private void MenuContextMerchant_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem Item in ListOnBudgetAccounts.SelectedItems)
                ((Account)Item.Tag).Type = "Merchant Account";

            UpdateUI();
        }

        private void MenuContextOnBudgetOpen_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem Item in ListOnBudgetAccounts.SelectedItems)
                ((Account)Item.Tag).Closed = false;

            UpdateUI();
        }

        private void MenuContextOnBudgetClosed_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem Item in ListOnBudgetAccounts.SelectedItems)
                ((Account)Item.Tag).Closed = true;

            UpdateUI();
        }

        //====================================================================================================
        //  Off budget account list click event handler.
        //====================================================================================================

        private void ListOffBudgetAccounts_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (ListOffBudgetAccounts.FocusedItem.Bounds.Contains(e.Location))
                {
                    bool FirstIteration = true;
                    bool MultipleType = false;
                    bool MultipleClosed = false;

                    var TypeMemory = "";
                    var ClosedMemory = false;

                    //----------------------------------------------------------------------------------------------------
                    //  Check all of the selected accounts to see if they have the same type and are all open/closed.
                    //----------------------------------------------------------------------------------------------------

                    foreach (ListViewItem Item in ListOffBudgetAccounts.SelectedItems)
                    {
                        if (FirstIteration)
                        {
                            TypeMemory = ((Account)Item.Tag).Type;
                            ClosedMemory = ((Account)Item.Tag).Closed;
                        }
                        else
                        {
                            if (TypeMemory != ((Account)Item.Tag).Type)
                                MultipleType = true;

                            if (ClosedMemory != ((Account)Item.Tag).Closed)
                                MultipleClosed = true;
                        }

                        FirstIteration = false;
                    }

                    //----------------------------------------------------------------------------------------------------
                    //  Set the correct account type checkbox if the selected account(s) is/are of the same type. Don't
                    //  set any checkboxes if accounts of multiple types are selected.
                    //----------------------------------------------------------------------------------------------------

                    if (MultipleType)
                    {
                        MenuContextMortgage.Checked = false;
                        MenuContextInvestment.Checked = false;
                        MenuContextOtherAsset.Checked = false;
                        MenuContextOtherLiability.Checked = false;
                    }
                    else
                    {
                        MenuContextMortgage.Checked = ((Account)ListOffBudgetAccounts.SelectedItems[0].Tag).Type == "Mortgage";
                        MenuContextInvestment.Checked = ((Account)ListOffBudgetAccounts.SelectedItems[0].Tag).Type == "Investment Account";
                        MenuContextOtherAsset.Checked = ((Account)ListOffBudgetAccounts.SelectedItems[0].Tag).Type == "Other Asset";
                        MenuContextOtherLiability.Checked = ((Account)ListOffBudgetAccounts.SelectedItems[0].Tag).Type == "Other Liability";
                    }

                    //----------------------------------------------------------------------------------------------------
                    //  Set the correct account open/closed checkbox if the selected account(s) is/are all open or closed.
                    //  Don't set any checkboxes if aa mixture of open/closed accounts are selected.
                    //----------------------------------------------------------------------------------------------------

                    if (MultipleClosed)
                    {
                        MenuContextOffBudgetOpen.Checked = false;
                        MenuContextOffBudgetClosed.Checked = false;
                    }
                    else
                    {
                        MenuContextOffBudgetOpen.Checked = !((Account)ListOffBudgetAccounts.SelectedItems[0].Tag).Closed;
                        MenuContextOffBudgetClosed.Checked = ((Account)ListOffBudgetAccounts.SelectedItems[0].Tag).Closed;
                    }

                    //----------------------------------------------------------------------------------------------------
                    //  Display the context menu.
                    //----------------------------------------------------------------------------------------------------

                    ContextOffBudgetAccount.Show(Cursor.Position);
                }
            }
        }

        //====================================================================================================
        //  Off budget account context menu click event handlers.
        //====================================================================================================

        private void MenuContextMortgage_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem Item in ListOffBudgetAccounts.SelectedItems)
                ((Account)Item.Tag).Type = "Mortgage";

            UpdateUI();
        }

        private void MenuContextInvestment_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem Item in ListOffBudgetAccounts.SelectedItems)
                ((Account)Item.Tag).Type = "Investment Account";

            UpdateUI();
        }

        private void MenuContextOtherAsset_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem Item in ListOffBudgetAccounts.SelectedItems)
                ((Account)Item.Tag).Type = "Other Asset";

            UpdateUI();
        }

        private void MenuContextOtherLiability_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem Item in ListOffBudgetAccounts.SelectedItems)
                ((Account)Item.Tag).Type = "Other Liability";

            UpdateUI();
        }

        private void MenuOffBudgetOpen_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem Item in ListOffBudgetAccounts.SelectedItems)
                ((Account)Item.Tag).Closed = false;

            UpdateUI();
        }

        private void MenuContextOffBudgetClosed_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem Item in ListOffBudgetAccounts.SelectedItems)
                ((Account)Item.Tag).Closed = true;

            UpdateUI();
        }
    }
}
