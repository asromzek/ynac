﻿namespace YNAC.Converter.Budget
{
    public class SubTransaction
    {
        public bool IsTransfer { get; set; }
        public bool IsIncome { get; set; }

        public Transaction Transaction { get; set; }
        public MasterCategory MasterCategory { get; set; }
        public Category Category { get; set; }
        public Payee Payee { get; set; }
        public int Inflow { get; set; }
        public int Outflow { get; set; }
        public string Memo { get; set; }
        public Account TransferAccount { get; set; }
    }
}
