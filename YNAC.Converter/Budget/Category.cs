﻿namespace YNAC.Converter.Budget
{
    public class Category
    {
        public string Name { get; set; }
        public MasterCategory MasterCategory { get; set; }

        public Category(string name, MasterCategory masterCategory)
        {
            Name = name;
            MasterCategory = masterCategory;
        }
    }
}
