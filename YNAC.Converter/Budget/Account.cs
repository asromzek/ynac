﻿using System.Collections.Generic;

namespace YNAC.Converter.Budget
{
    public class Account
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public bool OnBudget { get; set; }
        public bool Closed { get; set; }
        public int Balance { get; set; }
        public int SortIndex { get; set; }
        public List<Transaction> Transactions { get; set; }

        public Account(string name)
        {
            Name = name;
            OnBudget = false;
            Closed = false;
            Balance = 0;
            Transactions = new List<Transaction>();
        }
    }
}
