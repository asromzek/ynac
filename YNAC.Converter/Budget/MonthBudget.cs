﻿namespace YNAC.Converter.Budget
{
    public class MonthBudget
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public Category Category { get; set; }
        public int Amount { get; set; }

        public MonthBudget(int year, int month, Category category, int amount)
        {
            Year = year;
            Month = month;
            Category = category;
            Amount = amount;
        }
    }
}
