﻿using System;
using System.Collections.Generic;

namespace YNAC.Converter.Budget
{
    public class Transaction
    {
        private int _Inflow;
        private int _Outflow;

        public bool IsSplit { get; set; }
        public bool IsTransfer { get; set; }
        public bool IsIncome { get; set; }

        public Account Account { get; set; }
        public DateTime Date { get; set; }
        public Payee Payee { get; set; }
        public MasterCategory MasterCategory { get; set; }
        public Category Category { get; set; }
        public int Inflow
        {
            get
            {
                if (IsSplit)
                {
                    var Amount = 0;

                    foreach (var SubTransaction in SubTransactions)
                        Amount += SubTransaction.Inflow;

                    return Amount;
                }
                else
                    return _Inflow;
            }
            set { _Inflow = value; }
        }
        public int Outflow
        {
            get
            {
                if (IsSplit)
                {
                    var Amount = 0;

                    foreach (var SubTransaction in SubTransactions)
                        Amount += SubTransaction.Outflow;

                    return Amount;
                }
                else
                    return _Outflow;
            }
            set { _Outflow = value; }
        }
        public bool Cleared { get; set; }
        public bool Reconciled { get; set; }
        public string Flag { get; set; }
        public string Memo { get; set; }
        public List<SubTransaction> SubTransactions { get; set; }
        public Account TransferAccount { get; set; }

        public Transaction()
        {
            SubTransactions = new List<SubTransaction>();
        }
    }
}
