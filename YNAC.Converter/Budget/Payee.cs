﻿namespace YNAC.Converter.Budget
{
    public class Payee
    {
        public string Name { get; set; }

        public Payee(string name)
        {
            Name = name;
        }
    }
}
