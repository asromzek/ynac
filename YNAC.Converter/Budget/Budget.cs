﻿using System;
using System.Collections.Generic;
using YNAC.Financier;

namespace YNAC.Converter.Budget
{
    public class Budget
    {
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public CURRENCY Currency { get; set; }

        public Dictionary<string, Payee> Payees { get; set; }
        public Dictionary<string, MasterCategory> MasterCategories { get; set; }
        public Dictionary<string, Account> Accounts { get; set; }
        public Dictionary<string, MonthBudget> MonthBudgets { get; set; }

        public Budget(string name)
        {
            Payees = new Dictionary<string, Payee>();
            MasterCategories = new Dictionary<string, MasterCategory>();
            Accounts = new Dictionary<string, Account>();
            MonthBudgets = new Dictionary<string, MonthBudget>();
        }
    }
}
