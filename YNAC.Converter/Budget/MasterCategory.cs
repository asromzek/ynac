﻿using System.Collections.Generic;

namespace YNAC.Converter.Budget
{
    public class MasterCategory
    {
        public string Name { get; set; }
        public Dictionary<string, Category> Categories { get; set; }

        public MasterCategory(string name)
        {
            Name = name;
            Categories = new Dictionary<string, Category>();
        }
    }
}
